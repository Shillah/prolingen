(ns prolingen.test.project-test
  (:require [prolingen.project :refer :all :as proj]
            [prolingen.jobs :as jobs]

            [clojure.test :refer :all]
            [clojure.repl :refer :all]
            [clojure.set :as set]
            [tube-alloys.redef-methods :refer [with-redef-methods]]
            
            [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as gen]))

(def +NUM-SAMPLES+ 10)

(def project-strings [(str "(defproject p1 \"0.1.0\"" "\n  " ")") ; conform error. everthing is missing

                      "(defproject p2" ; read error. no closing )

                      (str "(defproject p3 \"1.0.0\"" "\n  "
                           ":license \"NONE\"" "\n  "
                           ":url \"example.com\"" "\n  "
                           ":run {main {:file \"file\", :goal goal, :args? true}
                                  main2 {:file [src \"file\"], :goal mod/goal :args? false}}" "\n  "
                           ":vendors [swipl sicstus]" "\n  "
                           ":dependencies [[origin/name \"version\"]]" ")") ; this works
                      ])

(deftest prolingen.project
  (with-redef-methods [jobs/do-job! {:prolingen.io/conform-error (fn [job acc]
                                                                   (let [data (-> job jobs/aux :data)]
                                                                     (jobs/return data [(jobs/terminate-job)])))
                                     :prolingen.jobs/error-msg (fn [job acc]
                                                                 (let [values (-> job jobs/aux :values)]
                                                                   (jobs/return values [(jobs/terminate-job)])))}]
    (let [jobs (mapv #(read-project-data-job % (fn [acc data] data)) project-strings)
          res (mapv #(jobs/job-loop (vector %)) jobs)]
      (testing "missing content"
        (let [res (nth res 0)]
          (is (map? res))))
      (testing "missing closing )"
        (let [res (nth res 1)]
          (is (map? res))
          (is (:exception res))))
      (testing "real thing"
        (let [res (nth res 2)]
          (is (map? res))
          (is (= "NONE" (:license res)))
          (is (= "example.com" (:url res)))
          (is (= ["main" "main2"] (vec (keys (:run res)))))
          (is (= [:swipl :sicstus] (:vendors res)))
          (is (= [{:name "name",
                   :origin "origin",
                   :version "version"}] (:dependencies res))))))))

(deftest prolingen.project.automated
  (with-redef-methods [jobs/do-job! {:prolingen.io/conform-error (fn [job acc]
                                                                   (let [string (-> job jobs/aux :string)
                                                                         data (-> job jobs/aux :data)]
                                                                     (s/explain-printer data)
                                                                     (jobs/return data [(jobs/terminate-job)])))
                                     :prolingen.jobs/error-msg (fn [job acc]
                                                                 (let [values (-> job jobs/aux :values)]
                                                                   (jobs/return values [(jobs/terminate-job)])))}]
    (let [input-projects (gen/sample (s/gen ::proj/project) +NUM-SAMPLES+)
          conformed-projects (mapv #(s/conform ::proj/project %) input-projects)
          projects (mapv conformed->project conformed-projects)
          project-strs (mapv project->string projects)
          jobs (mapv #(read-project-data-job % (fn [acc data] data)) project-strs)
          res (mapv #(jobs/job-loop (vector %)) jobs)
          ]
      (testing "Automated project testing"
        (is (= projects res))))))
