(ns prolingen.test.verify-dependencies-test
  (:require [prolingen.verify-dependencies :refer :all]
            [prolingen.jobs :as jobs]

            [clojure.test :refer :all]
            [clojure.repl :refer :all]
            [clojure.spec.alpha :as s]
            [clojure.set :as set]
            [tube-alloys.redef-methods :refer [with-redef-methods]]))

(def graphs [{1 #{2},
              2 #{3},
              3 #{},
              4 #{}}, ; no cycle

             {1 #{2},
              2 #{3},
              3 #{},
              4 #{6},
              5 #{4 6},
              6 #{}}  ; no cycle

             {1 #{2},
              2 #{3},
              3 #{},
              4 #{6},
              5 #{4 6},
              6 #{1}} ; no cycle
             
             {1 #{2},
              2 #{3},
              3 #{},
              4 #{6},
              5 #{4 6},
              6 #{4}} ; cycle 4->6->4

             {1 #{2},
              2 #{3 5},
              3 #{},
              4 #{6},
              5 #{4 6},
              6 #{1}} ; cycle 1 -> 2 -> 5 (-> 4) -> 6 -> 1

             {1 #{2},
              2 #{3 5},
              3 #{4},
              4 #{2 6},
              5 #{4 6},
              6 #{1}} ; a lot of cycles
             ])

(defn same-cycle [cycle1 cycle2]
  (and (= (set cycle1)
          (set cycle2))
       (let [start1     (first cycle1)
             inf-cycle2 (flatten (repeat cycle2))
             cycle2'    (drop-while #(not= start1 %) inf-cycle2)
             cycle2'    (take (count cycle1) cycle2')]
         (= (vec cycle1)
            (vec cycle2')))))

(defn get-cycle [start walked]
  (loop [cycle (list start)
         current start]
    (let [n (get walked current)]
      (if (= n start)
        (vec cycle)
        (recur (conj cycle n)
               n)))))

(deftest prolingen.verify-dependencies.cycle-check
  (with-redef-methods [jobs/do-job! {::jobs/error-msg (fn [job acc]
                                                        (let [start  (-> job jobs/aux :values :start)
                                                              walked (-> job jobs/aux :values :walked)
                                                              cycle  (get-cycle start walked)]
                                                          (jobs/return cycle [])))}]
    (let [jobs (mapv cycle-check-job graphs)
          res  (mapv #(jobs/job-loop (vector %)) jobs)]
      (testing "No Cycle (Easy)"
        (let [res (nth res 0)]
          (is (= res nil))))
      (testing "No Cycle (Normal)"
        (let [res (nth res 1)]
          (is (= res nil))))
      (testing "No Cycle (Hard)"
        (let [res (nth res 2)]
          (is (= res nil))))
      (testing "Cycle (Easy)"
        (let [res (nth res 3)]
          (is (vector? res))
          (is (same-cycle res [4 6]))))
      (testing "Cycle (Normal)"
        (let [res (nth res 4)]
          (is (vector? res))
          (is (or (same-cycle res [1 2 5 6])
                  (same-cycle res [1 2 5 4 6])))))
      (testing "Cycle (Hard)"
        (let [res (nth res 5)]
          (is (vector? res))
          (is (or (same-cycle res [2 5 4])
                  (same-cycle res [2 5 4 6 1])
                  (same-cycle res [2 5 6 1])
                  (same-cycle res [2 3 4])
                  (same-cycle res [2 3 4 6 1]))))))))


(def A1 {:name "A", :version "1"})
(def A2 {:name "A", :version "2"})
(def A3 {:name "A", :version "3"})
(def B1 {:name "B", :version "1"})
(def B2 {:name "B", :version "2"})
(def B3 {:name "B", :version "3"})
(def C1 {:name "C", :version "1"})
(def C2 {:name "C", :version "2"})
(def C3 {:name "C", :version "3"})

(def AA1 (assoc A1 :origin "A"))
(def AA2 (assoc A2 :origin "A"))
(def AA3 (assoc A3 :origin "A"))
(def BB1 (assoc B1 :origin "B"))
(def BB2 (assoc B2 :origin "B"))
(def BB3 (assoc B3 :origin "B"))
(def AC1 (assoc C1 :origin "A"))
(def BC1 (assoc C1 :origin "B"))
(def CC2 (assoc C2 :origin "C"))
(def CC3 (assoc C3 :origin "C"))

(def test-dependencies {AA1 {:dependencies [AC1],
                             :modules      ["A1"],
                             :vendors      [:swipl, :sicstus]}
                        BB1 {:dependencies [BC1],
                             :modules      ["B1"],
                             :vendors      [:swipl :sicstus]}
                        AC1 {:dependencies [],
                              :modules      ["C1"],
                              :vendors      [:swipl]}
                        BC1 {:dependencies [],
                             :modules      ["C1"],
                             :vendors      [:swipl]}
                        AA2 {:dependencies [CC2],
                             :modules      ["A2"],
                             :vendors      [:swipl :sicstus]}
                        BB2 {:dependencies [CC2],
                             :modules      ["B2"],
                             :vendors      [:swipl :sicstus]}
                        CC2 {:dependencies [],
                             :modules      ["C2"],
                             :vendors      [:swipl :sicstus]}
                        AA3 {:dependencies [],
                             :modules      ["A3"],
                             :vendors      [:swipl :sicstus]}
                        BB3 {:dependencies [CC3],
                             :modules      ["B3"],
                             :vendors      [:swipl :sicstus]}
                        CC3 {:dependencies [BB3],
                             :modules      ["C3"],
                             :vendors      [:swipl :sicstus]}})

(defn load-dependency [dep]
  (merge dep (test-dependencies dep)))

(deftest prolingen.verify-dependencies.verify-deps
  (with-redef-methods [jobs/do-job! {:prolingen.verify-dependencies/load (fn [job acc]
                                                                           (let [dep       (-> job jobs/aux :dep)
                                                                                 transform (-> job jobs/aux :transform)
                                                                                 loaded-dep (load-dependency dep)]
                                                                             (jobs/return (transform acc loaded-dep) [])))
                                     :prolingen.verify-dependencies/check-files (fn [job acc]
                                                                                  (jobs/return acc []))
                                     ::jobs/error-msg (fn [job acc]
                                                        (let [values (-> job jobs/aux :values)]
                                                          (jobs/return values [(jobs/terminate-job)])))}]
    (let [inputs [["can be ignored" [AA1] ["Y"] #{:swipl} #(identity %2)]     ; should work
                  ["can be ignored" [AA1 BB1] ["Y"] #{:swipl} #(identity %2)] ; should work
                  ["can be ignored" [AA1] ["A1"] #{:swipl} #(identity %2)]    ; module clash with A1
                  ["can be ignored" [AA1] ["C1"] #{:swipl} #(identity %2)]    ; module clash with C1
                  ["can be ignored" [AA1] ["C1"] #{:sicstus} #(identity %2)]  ; no vendors
                  
                  ["can be ignored" [AA1 BB2] ["Y"] #{:swipl} #(identity %2)] ; version error
                  ["can be ignored" [AA2 BB2] ["Y"] #{:swipl} #(identity %2)] ; should work
                  ["can be ignored" [AA1 AA3] ["Y"] #{:swipl} #(identity %2)] ; version error
                  ["can be ignored" [BB3] ["Y"] #{:swipl} #(identity %2)]     ; cycle error
                  ]
          
          jobs   (mapv #(apply verify-dependencies-job %) inputs)
          res  (mapv #(jobs/job-loop (vector %)) jobs)]
      (testing "AA1"
        (let [res (nth res 0)]
          (is (= (set res) #{A1 C1}))))
      (testing "AA1 BB1"
        (let [res (nth res 1)]
          (is (= (set res) #{A1 B1 C1}))))
      (testing "A1 nameclash"
        (let [res (nth res 2)]
          (is (map? res))
          (is (= (:mod res) "A1"))
          (is (= (:dep-name res) "A"))))
      (testing "C1 nameclash"
        (let [res (nth res 3)]
          (is (map? res))
          (is (= (:mod res) "C1"))
          (is (= (:dep-name res) "C"))))
      (testing "No vendor (C1 Needs swipl)"
        (let [res (nth res 4)]
          (is (map? res))
          (is (= (:dep res) (load-dependency AC1)))))
      (testing "AA1 BB2 version error (C1 & C2)"
        (let [res (nth res 5)]
          (is (map? res))
          (is (= #{(:loaded-version res)
                   (:version res)} #{"1" "2"}))))
      (testing "AA2 BB2"
        (let [res (nth res 6)]
          (is (= (set res) #{A2 B2 C2}))))
      (testing "AA1 AA3 version error (A1 & A3)"
        (let [res (nth res 7)]
          (is (map? res))
          (is (= #{(:loaded-version res)
                   (:version res)} #{"1" "3"}))))
      (testing "B3 cycle error (B3 -> C3 -> B3)"
        (let [res (nth res 8)]
          (is (map? res))
          (let [start (:start res)
                walked (:walked res)
                cycle (get-cycle start walked)]
            (is (same-cycle cycle ["B" "C"]))))))))
