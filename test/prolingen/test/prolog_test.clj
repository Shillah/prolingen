(ns prolingen.test.prolog-test
  (:require [prolingen.prolog :refer :all :as prol]
            [prolingen.jobs :as jobs]
            [prolingen.io   :as io]

            [clojure.test :refer :all]
            [clojure.repl :refer :all]
            [clojure.spec.alpha :as s]
            [clojure.set :as set]
            [tube-alloys.redef-methods :refer [with-redef-methods]]))

(def A (io/make-path :file ["A.pl"]))
(def A2 (io/make-path :file ["A2.pl"]))
(def B (io/make-path :file ["B.pl"]))
(def C (io/make-path :file ["C.pl"]))
(def D (io/make-path :file ["D.pl"]))
(def E (io/make-path :file ["E.pl"]))
(def F (io/make-path :file ["F"]))

(def files {A ":- module(A).", ; ok
            A2 ":- module(A).", ; ok
            B "no module" ; ok
            C ":- module C." ; nope
            D ":-module( D , [oueuo oi oei o/2] )." ; ok
            E ":- module(E", ; nope
            F ":- module(F)." ; technically ok, but F is no prolog file
            })

(defn loadf [path]
  (files path))

(deftest prolingen.prolog
  (with-redef-methods [jobs/do-job! {::io/load-file (fn [job acc]
                                                      (let [path      (-> job jobs/aux :file)
                                                            transform (-> job jobs/aux :transform)]
                                                        (jobs/return (transform acc (loadf path)) [])))
                                     ::jobs/error-msg (fn [job acc]
                                                        (let [values (-> job jobs/aux :values)]
                                                          (jobs/return values [(jobs/terminate-job)])))
                                     }]
    (let [pathss [[A] ; ok
                  [A A2] ; double module A
                  [C] ; X
                  [E] ; X
                  [A B D] ; ok
                  [F]
                  [A B D F]
                  ]
          jobs (mapv #(module-declaration-from-paths-job % (fn [acc data] data)) pathss)
          res (mapv #(jobs/job-loop (vector %)) jobs)]
      (testing "A"
        (let [res (nth res 0)]
          (is (= #{"A"} res))))
      (testing "A A2"
        (let [res (nth res 1)]
          (is (map? res))
          (is (= (count (:clashes res)) 1)) ; one clash
          (is (= (first (first (:clashes res))) "A")) ; clash with module A
          (is (= (set (second (first (:clashes res)))) ; clash caused by A.pl and A2.pl
                 #{"A.pl" "A2.pl"}))))
      (testing "C"
        (let [res (nth res 2)]
          (is (= #{} res))))
      (testing "E"
        (let [res (nth res 3)]
          (is (= #{} res))))
      (testing "A B D"
        (let [res (nth res 4)]
          (is (= #{"A" "D"} res))))
      (testing "F"
        (let [res (nth res 5)]
          (is (= #{} res))))
      (testing "A B D F"
        (let [res (nth res 6)]
          (is (= #{"A" "D"} res)))))))
