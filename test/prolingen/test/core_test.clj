(ns prolingen.test.core-test
  (:require [prolingen.core :refer :all]
            [prolingen.jobs :as jobs]

            [clojure.test :refer :all]
            [clojure.repl :refer :all]
            [clojure.spec.alpha :as s]))

(deftest prolingen.core
  (let [inputs [["--prefix" "build" "swipl" "a"] ; prefix is missing
                ["--prefix" "a" "build" "sicstus" "a"] ; sicstus is not a build system
                ["run"] ; run goal is missing
                ["test"] ; test needs at least 1 argument
                ["hyperpl" "test"] ; is ok
                ["haha" "xD"] ; haha is not a valid command
                ["--prefix" "test" "--vendor" "v" "info"] ; is ok
                ["info" "arg1"] ; info does not expect arguments
                ]
        jobs (mapv #(validate-command-line-job % (fn [acc flags command] {:flags flags,
                                                                         :command command})) inputs)
        res  (mapv #(let [writer (new java.io.StringWriter)]
                      (binding [*out* writer]
                        (-> {}
                            (assoc :res (jobs/job-loop (vector %)))
                            (assoc :out (str writer))))) jobs)]

    ; nil = Error was thrown
    (testing "--prefix build swipl a"
      (is (= nil (-> res (nth 0) :res))))
    (testing "--prefix a build swipl a"
      (is (= nil (-> res (nth 1) :res))))
    (testing "run"
      (is (= nil (-> res (nth 2) :res))))
    (testing "test"
      (is (= nil (-> res (nth 3) :res))))
    (testing "hyperpl test"
      (is (= {:flags {},
              :command {:command :prolingen.command-api/hyperpl,
                        :arguments "test"}} (-> res (nth 4) :res))))
    (testing "haha xD"
      (is (= nil (-> res (nth 5) :res))))
    (testing "--prefix test info"
      (is (= {:flags {:prefix "test",
                      :vendor "v"},
              :command {:command :prolingen.command-api/info,
                        :arguments nil}} (-> res (nth 6) :res))))
    (testing "info arg1"
      (is (= nil (-> res (nth 7) :res))))))
