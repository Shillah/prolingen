(ns prolingen.test.settings-test
  (:require [prolingen.settings :refer :all :as sett]
            [prolingen.jobs :as jobs]
            [prolingen.io   :as io]

            [clojure.test :refer :all]
            [clojure.repl :refer :all]
            [clojure.set :as set]
            [tube-alloys.redef-methods :refer [with-redef-methods]]

            [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as gen]))

(def +NUM-SAMPLES+ 10)

(def setting-strings [(str "{}") ; conform error. everthing is missing
                      
                      "{" ; read error. no closing )
                      
                      (str "{:vendors {swipl {:location \"string\", :type swipl}}" "\n  "
                           ":default-vendor swipl" "\n  "
                           ":locations {}" "}") ; this works
                      ])
(deftest prolingen.prolog
  (with-redef-methods [jobs/do-job! {:prolingen.io/conform-error (fn [job acc]
                                                                   (let [data (-> job jobs/aux :data)]
                                                                     (jobs/return data [(jobs/terminate-job)])))
                                     ::jobs/error-msg (fn [job acc]
                                                        (let [values (-> job jobs/aux :values)]
                                                          (jobs/return values [(jobs/terminate-job)])))
                                     }]
    (let [jobs (mapv #(read-settings-from-string-job % false (fn [acc data] data)) setting-strings)
          res (mapv #(jobs/job-loop (vector %)) jobs)]
      (testing "missing content"
        (let [res (nth res 0)]
          (is (map? res))))
      (testing "missing closing )"
        (let [res (nth res 1)]
          (is (map? res))
          (is (:exception res))))
      (testing "real thing"
        (let [res (nth res 2)]
          (is (map? res))
          (is (= {} (:locations res)))
          (is (= :swipl (:default-vendor res)))
          (is (= {:swipl {:location "string", :type :swipl}} (:vendors res))))))))

(defn settings->string [settings]
  (-> settings settings->conformed conformed->unformed str))

(deftest prolingen.settings.automated
  (with-redef-methods [jobs/do-job! {:prolingen.io/conform-error (fn [job acc]
                                                                   (let [string (-> job jobs/aux :string)
                                                                         data (-> job jobs/aux :data)]
                                                                     (jobs/return data [(jobs/terminate-job)])))
                                     :prolingen.jobs/error-msg (fn [job acc]
                                                                 (let [values (-> job jobs/aux :values)]
                                                                   (jobs/return values [(jobs/terminate-job)])))}]
    (let [input-settings (gen/sample (s/gen ::sett/global-settings) +NUM-SAMPLES+)
          conformed-settings (mapv #(s/conform ::sett/global-settings %) input-settings)
          settings (mapv conformed->settings conformed-settings)
          setting-strs (mapv settings->string settings)
          jobs (mapv #(read-settings-from-string-job % false (fn [acc data] data)) setting-strs)
          res (mapv #(jobs/job-loop (vector %)) jobs)
          ]
      (testing "Automated setting testing"
        (is (= settings res))))))
