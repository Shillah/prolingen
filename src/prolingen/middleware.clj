(ns prolingen.middleware)

(defn- transform-goal [goal]
  (if (:module goal)
    [(:module goal) (:goal goal) (if (:args? goal) "1" "0")]
    [(:goal goal) (if (:args? goal) "1" "0")]))

(defn- transform-aliases [aliases]
  (mapcat (fn [[alias paths]] (interleave (repeat (name alias)) paths)) aliases))

(defn transform-files
  "The files are supplied either as actual filenames, or as vectors [alias-name, identifier].
  This function transforms them into a form that the `run.pl` file expects them, i.e.
  either as actual file names or as \"sequence\" alias-name identifier "
  [files]
  (butlast (mapcat (fn [file] (if (string? file)
                               [file "-"]
                               ;; otherwise we already have a vector
                               (into file ["-"])))
                   files)))

(defn run-arguments [goal aliases files arguments]
  (let [transformed-goal    (transform-goal goal)
        transformed-aliases (transform-aliases aliases)
        transformed-files   (transform-files files)]
    (vec (concat transformed-goal
                 ["--"]
                 transformed-aliases
                 ["--"] ; libraries to add
                 transformed-files
                 ["--"]
                 arguments))))

(defn repl-arguments [aliases]
  (vec (transform-aliases aliases)))
