(ns prolingen.io
  (:require [prolingen.jobs   :as jobs]
            [clojure.java.io :as jo]
            [clojure.string  :as s]
            [clojure.spec.alpha :as spec])
  (:import (java.util.zip ZipOutputStream
                          ZipEntry)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SOME HELPER FUNCTIONS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn mega-merge [default override]
  (cond
    (and (sequential? default)
         (sequential? override)) (vec (concat default override))
    (and (map?        default)
         (map?        override)) (let [all-keys (concat (keys default) (keys override))]
                                   (reduce (fn [acc key]
                                             (let [def-val  (get default key  ::no-val)
                                                   over-val (get override key ::no-val)
                                                   real-val (cond
                                                              (= ::no-val def-val)  over-val
                                                              (= ::no-val over-val) def-val
                                                              :else                 (mega-merge def-val over-val))]
                                               (assoc acc key real-val)))
                                           {} all-keys))
    :else                        override))

(def ^:dynamic *system*
  {:name     (System/getProperty "os.name")
   :version  (System/getProperty "os.version")
   :arch     (System/getProperty "os.arch")})

(defn is-windows? []
  (let [name (s/lower-case (:name *system*))]
    (s/includes? name "win")))

(defn is-mac? []
  (let [name (s/lower-case (:name *system*))]
    (s/includes? name "mac")))

(defn is-unix? []
  (let [name (s/lower-case (:name *system*))]
    (or (s/includes? name "nix")
        (s/includes? name "nux")
        (s/includes? name "aix"))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PROGRAM-JOB
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn enable-options! [pb {:keys [working-directory inherit-io redirect-error]}]
  (cond-> pb
    working-directory (.directory working-directory)
    inherit-io        (.inheritIO)
    redirect-error    (.redirectErrorStream redirect-error)))

(defn run-prog2 [prog-name prog-args options]
  (let [args (cons prog-name prog-args)
        ensure-strings (mapv str args)
        pb (ProcessBuilder. (into-array String ensure-strings))]
    #_(do (print "[STARTING]" prog-name "")
        (doseq [arg prog-args] (print arg ""))
        (println)
        (println options))
    (enable-options! pb options)
    (let [p (.start pb)]
      (.waitFor p))))

(defn run-prog [prog-name prog-args & {:as options}]
  (run-prog2 prog-name prog-args options))

(defn program-job [prog-name prog-args & {:as options}]
  (jobs/new-job ::program {:name prog-name,
                           :args prog-args,
                           :options options}))

(defmethod jobs/do-job! ::program [job acc]
  (let [aux (jobs/aux job)
        pname (:name aux)
        pargs (:args aux)
        popt  (:options aux)]
    (run-prog2 pname pargs popt)
    (jobs/return acc [])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PATH DEFINITION & FUNCTIONS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; TODO: change path from array to map {:type type, :data data}
(defn path-type [path] (first path))
(defn path-data [path] (second path))
(defn make-path [type data] [type data])

(defn as-path [type string] (make-path type [string]))
(defn as-dir-path [string] (as-path :dir string))
(defn as-file-path [string] (as-path :file string))

(defn add-to-path
  "Creates a new path, by concatenating `path`s data with `to-add` and changing the type
  to `new-type`. If `new-type` is not provided, `path`s type is used."
  ([path to-add] (add-to-path path to-add (path-type path)))
  ([path to-add new-type]
   (let [data (path-data path)]
     (make-path new-type (into data to-add)))))

(defn add-prefix
  [path prefix]
  (let [data (path-data path)
        type (path-type path)]
    (make-path type (into [prefix] data))))

(defn merge-paths
  ([start finish] (merge-paths start finish (path-type finish)))
  ([start finish type] (add-to-path start (path-data finish) type)))

(defn dir-path?
  "Returns true if the path is supposed to be a directory."
  [path]
  (= :dir (path-type path)))

(def fsep (java.io.File/separator))

(defn path->name
  "Returns the filename corresponding to this path."
  [path]
  (let [name (s/join fsep (path-data path))]
    (if (dir-path? path)                ; if path is a path to a directory, we also add a seperator to the end of the name
      (str name fsep)
      name)))

(defn path->file
  "Return the file corresponding to this path. This function does not retain the information
  whether this path was a file-path or a directory-path."
  [path]
  ;; for convenience this function also accepts strings & java.io.Files
  (cond
    (string? path)                (jo/file path)
    (instance? java.io.File path) path
    :else                         (jo/file (path->name path))))

(defn path->absolute-name
  "Returns the absolute filename corresponding to this path."
  [path]
  (let [name (.getAbsolutePath (path->file path))]
    (if (dir-path? path)
      (str name fsep)
      name)))

(defn ensure-path-exists!
  "If the file/directory corresponding to path does not yet exist, create it.
   If a file/directory corresponding to the path does exist, but is of the wrong type,
   this function returns a false value.

  If the file/directory exists or was created, return the file corresponding to the path."
  [path]
  (let [type (path-type path)
        f (path->file path)]
    (if (.exists f)
      ;; check wether it has the right type
      (or (and (= type :dir)
               (.isDirectory f)
               f)
          (and (not= type :dir)
               (.isFile f)
               f))
      ;; if it doesnt exist, create it
      (and
       ;; first we need to create the parent directories
       (if-let [parent (.getParentFile f)]
         ;; since mkdirs returns false if all parents exist,
         ;; we also have to check wether the parent already exists
         (or (.exists parent) (.mkdirs parent))
         true)
       ;; then we create the file/dir
       (if (= type :dir)
         (.mkdir f)
         (.createNewFile f))
       ;; if everything worked, we return the file
       f))))

(defn ensure-paths-exist
  "Applies `ensure-path-exists!` on all paths."
  [paths]
  (reduce (fn [acc path]
              (and acc (ensure-path-exists! path)))
            true paths))

(defn file->data
  "Read out the contents of a file."
  [file]
  (slurp file))

(defn path->data
  "Reads out the contents of a path.  If `path` does not correspond to a
  file, `nil` is returned instead."
  [path]
  (if (= (path-type path) :file)
    (file->data (path->file path))))

(def prolingen-tree
  {:home {".prol" {"settings.clj" :file
                   "scripts" :dir
                   "repositories" :dir}}})

(defn tree->paths
  "Transforms a directory tree into a list of paths. Each path has the form
  `[type [part1 part2 ...]]` where type is either `:file` or `:dir` and `part1`, `part2`, etc.
  are the parts of the path. `subs` is a map of substitutions, if non is supplied, default
  substitutions take place:
  :home -> users home directory
  "
  ([tree] (tree->paths tree {:home (java.lang.System/getProperty "user.home")}))
  ([tree subs] (tree->paths tree subs [] []))
  ([tree subs root acc]
   (cond
     (map?     tree) (let [new-part #(subs % %)] ; tries to substitute the part of the path
                       (reduce (fn [acc [k v]] (tree->paths v subs (conj root (subs k k))
                                                           acc)) acc tree))
     ;; at this point, root should be the complete path
     ;; and root should be the type, so we just return the
     ;; completed path
     ;; (dont forget that tree->paths returns a list of paths!)
     (keyword? tree) (conj acc [tree root]))))


(def prolingen-paths      (tree->paths prolingen-tree))
(def settings-path        (nth prolingen-paths 0))
(def scripts-path         (nth prolingen-paths 1))
(def repl-file-path       (add-to-path scripts-path ["repl.pl"] :file))
(def create-sav-file-path       (add-to-path scripts-path ["create-sav.pl"] :file))
(def run-file-path        (add-to-path scripts-path ["run.pl"] :file))
(def repository-path      (nth prolingen-paths 2))

(def prolingen-filenames (mapv path->name prolingen-paths))

(defn is-installed? []
  (let [files (mapv jo/file prolingen-filenames)]
    (every? #(.exists %) files)))


(defn source-prefix [info]
  (let [name   (get-in info [:prj :name])
        prefix (get-in info [:flags :prefix])
        data   (if prefix [prefix "src"] ["src"])]
    (path->name (make-path :dir data))))

;;; TODO: this should be renamed, and should also return a PATH !!!
(defn get-source-path [info]
  (assert false)
  (let [name (get-in info [:prj :name])
        prefix (get-in info [:flags :prefix])
        data   (if prefix [prefix "src" name] ["src" name])]
    (path->file (make-path :dir data))))

(defn source-path [info]
  (let [name   (get-in info [:prj :name])
        prefix (get-in info [:flags :prefix])
        data   (if prefix [prefix "src" name] ["src" name])]
    (make-path :dir data)))

(defn root-path [info]
  (let [name   (get-in info [:prj :name])
        prefix (get-in info [:flags :prefix])
        data   (if prefix [prefix "."] ["."])]
    (make-path :dir data)))

(defn test-path [info]
  (let [name   (get-in info [:prj :name])
        prefix (get-in info [:flags :prefix])
        data   (if prefix [prefix "test"] ["test"])]
    (make-path :dir data)))

(defn build-path [info]
  (let [name   (get-in info [:prj :name])
        prefix (get-in info [:flags :prefix])
        data   (if prefix [prefix "build"] ["build"])]
    (make-path :dir data)))

(defn- get-name [path]
  (-> path path->file .getAbsolutePath))

(defn default-aliases [info]
  (let [src-name   (get-name (source-path info))
        root-name  (get-name (root-path info))
        test-name  (get-name (test-path info))
        build-name (get-name (build-path info))]
    {:src [src-name],
     :root [root-name],
     :test [test-name],
     :build [build-name]}))

(defn dep->path
  "Given {:name <dep-name>, :version <dep-version>}, returns a path to that dependency"
  [dep]
  (if-let [dep-org (:dependency/org dep)]
    (let [dep-name (:dependency/name dep)
          dep-ver  (:dependency/version dep)]
      ;; libraries are saved as <rep-path>/<dep-org>/<dep-name>/<dep-version>
      (add-to-path repository-path (into dep-org [dep-name dep-ver])))
    (let [dep-name (:dependency/name dep)
          dep-ver  (:dependency/version dep)]
      ;; libraries are saved as <rep-path>/<dep-name>/<dep-version>
      (add-to-path repository-path [dep-name dep-ver]))))

(defn directory->tree*
  [^java.io.File dir]
  (let [files (.listFiles dir)]
    (reduce (fn [acc ^java.io.File file]
              (if (.isDirectory file)
                (assoc acc (.getName file) (directory->tree* file))
                (assoc acc (.getName file) :file)))
            {}
            files)))

(defn directory->tree [dir]
  (let [f (path->file dir)]
    (when (.isDirectory f)
      {(.getName f) (directory->tree* f)})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; NEW-FILE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn new-file-job
  "`file-path` is a path to a file or directory.  `contents` is the content that is to be written into
  the newly created file."
  ([file-path] (new-file-job file-path nil))
  ([file-path contents] (jobs/new-job ::new-file {:path file-path,
                                                  :contents contents})))

(defn- mkdirs-job
  "Creates the directory named by this abstract pathname, including any necessary but nonexistent parent directories."
  [path & {:keys [already-exists]
           :or   {already-exists jobs/ignore-job}}]
  (jobs/new-job ::mkdirs {:path path,
                          :already-exists already-exists}))

(defn- mkdir-job
  "Creates the directory named by this abstract pathname. All parents have to exist."
  [path & {:keys [already-exists]
           :or   {already-exists jobs/ignore-job}}]
  (jobs/new-job ::mkdir {:path path,
                         :already-exists already-exists}))

(defn- spit-job [file contents]
  (jobs/new-job ::spit {:file file,
                        :contents contents}))

(defmethod jobs/do-job! ::new-file [job acc]
  (let [aux (jobs/aux job)
        path (:path aux)
        ptype (path-type path)
        file (path->file path)
        contents (:contents aux)
        parent (.getParentFile file)
        jobs [(mkdirs-job parent)
              (if (= ptype :dir)
                (mkdir-job file)
                (spit-job file contents))]]
    (jobs/return acc (if (and (= ptype :dir)
                              (not (nil? contents)))
                       (into jobs [(jobs/warning-job (str "Tried to create the dir '" (.getPath file) "' but specified contents.")
                                                     {:path (.getPath file)})])
                       jobs))))

(defmethod jobs/do-job! ::mkdirs [job acc]
  (let [file (-> job jobs/aux :path)
        already-exists (-> job jobs/aux :already-exists)]
    (if (.exists file)
      (jobs/return acc [(already-exists (str "Tried to create dirs " (.getPath file) ", but it alreday exists.")
                                        {:path (.getPath file)})])
      (try
        (do
          (.mkdirs file)
          (jobs/return acc []))
        (catch Exception e
          (jobs/return acc [(jobs/error-job (.getMessage e) {:exception e,
                                                             :job job,
                                                             :file file})]))))))

(defmethod jobs/do-job! ::mkdir [job acc]
  (let [file (-> job jobs/aux :path)
        already-exists (-> job jobs/aux :already-exists)]
    (if (.exists file)
      (jobs/return acc [(already-exists (str "Tried to create dirs '" (.getPath file) "', but it alreday exists.")
                                        {:path (.getPath file),
                                         :job job})])
      (try
        (do
          (.mkdir file)
          (jobs/return acc []))
        (catch Exception e
          (jobs/return acc [(jobs/error-job (.getMessage e) {:exception e, :job job, :file file})]))))))

(defmethod jobs/do-job! ::spit [job acc]
  (let [file     (-> job jobs/aux :file)
        contents (-> job jobs/aux :contents)]
    (try
      (do
        (spit file contents)
        (jobs/return acc []))
      (catch Exception e
        (jobs/return acc [(jobs/error-job (.getMessage e) {:exception e, :contents contents, :file file, :job job})])))))


(defn ensure-path-exists-jobs
  "If the file/directory corresponding to path does not yet exist, returns a job to create it.
   If a file/directory corresponding to the path does exist, but is of the wrong type,
  returns an error job.

  Returns a vector of jobs to do."
  [path]
  (let [type (path-type path)
        f (path->file path)]
    (if (.exists f)
      ;; if the file exists,
      ;; check wether it has the right type
      (if (or (and (= type :dir)
                   (.isDirectory f))
              (and (not= type :dir)
                   (.isFile f)))
        ;; everything is fine. nothing to do.
        []
        ;; its of the wrong type, return an error
        (if (= type :dir)
          [(jobs/error-job (str "'" (path->name path) "' already exists, but is not a directory.") {:name (path->name path)})]
          [(jobs/error-job (str "'" (path->name path) "' already exists, but is not a file.") {:name (path->name path)})]))
      ;; if it doesnt exist, create it
      [(new-file-job path)])))

(defn create-paths-job [paths]
  (jobs/new-job ::create-paths paths))

(defmethod jobs/do-job! ::create-paths [job acc]
  (let [paths (jobs/aux job)
        jobs  (->> paths (map ensure-path-exists-jobs) (reduce concat) vec)]
    (if (not-any? jobs/error-job? jobs)
      (jobs/return acc jobs)
      ;; we want to print all errors, so we first tell all errors not to throw (i.e. stop execution)
      ;; and then terminate it ourselves without printing anything.
      (let [errors (filter jobs/error-job? jobs)
            no-throw (mapv (fn [e] (jobs/transform-aux e #(assoc % :throw false))) errors)]
        (jobs/return acc (into no-throw [(jobs/terminate-job)]))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PATH-EXISTS?
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn path-exists?-job [path & {:keys [file-not-found wrong-type]
                                :or   {file-not-found jobs/error-job
                                       wrong-type     jobs/warning-job}}]
  (jobs/new-job ::path-exists {:path path,
                               :not-found file-not-found,
                               :wrong     wrong-type}))

(defmethod jobs/do-job! ::path-exists [job acc]
  (let [path      (-> job jobs/aux :path)
        not-found (-> job jobs/aux :not-found)
        wrong     (-> job jobs/aux :wrong)
        name      (path->name path)
        file      (path->file path)
        type      (path-type path)
        type-str  (if (= :dir type) "directory" "file")]
    (cond
      (not (.exists file)) (jobs/return acc [(not-found (str "The file '" name "' does not exist.") {:path path,
                                                                                                     :file file,
                                                                                                     :name name})])
      (not (or (and (= :dir type)
                    (.isDirectory file))
               (and (not= :dir type)
                    (.isFile file))))  (jobs/return acc [(wrong (str "We expected '" name "' to be a " type-str
                                                                     ", but it was not.")
                                                                {:path path, :file file, :name name})])
      :else               (jobs/return acc []))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; WRITE-FILE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn write-file*-job
  [file content & {:keys [write-exception]
                   :or   {write-exception jobs/error-job}}]
  (jobs/new-job ::write-file* {:file file,
                               :content content,
                               :exception write-exception}))

(defmethod jobs/do-job! ::write-file* [job acc]
  (let [p  (-> job jobs/aux :file)
        c  (-> job jobs/aux :content)
        ex (-> job jobs/aux :exception)
        f  (path->file p)]
    (try
      (do
        (spit f c)
        (jobs/return acc []))
      (catch Exception e
        (let [name (path->name p)]
          (jobs/return acc [(ex (str "Encountered an unexpected exception while writing to file '" name
                                     "' (" (.getMessage e) ").")
                                {:name name,
                                 :file f,
                                 :path p,
                                 :exception e})]))))))

(defn write-file-job [file content & {:keys [already-exists write-exception wrong-type]
                                      :or   {already-exists   jobs/warning-job
                                             wrong-type       jobs/error-job
                                             write-exception   jobs/error-job}}]
  (jobs/new-job ::write-file {:file      file,
                              :content   content
                              :exists    already-exists,
                              :wrong     wrong-type
                              :exception write-exception}))

(defmethod jobs/do-job! ::write-file [job acc]
  (let [path       (-> job jobs/aux :file)
        content    (-> job jobs/aux :content)
        exists     (-> job jobs/aux :exists)
        wrong      (-> job jobs/aux :wrong)
        exception  (-> job jobs/aux :exception)
        f          (path->file path)
        name       (path->name path)
        exists?    (.exists f)]
    (if (.isDirectory f)
      (jobs/return acc [(wrong (str name " already exists and is a directory, so it cannot be written too.") {:path path,
                                                                                                              :file f,
                                                                                                              :name name})])
      (if exists?
        (jobs/return acc [(exists (str name " already exists.") {:path path, :file f, :name name})
                          (write-file*-job path content :write-exception exception)])
        (jobs/return acc [(write-file*-job path content :write-exception exception)])))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LOAD-FILE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn load-file-job [file acc-transform & {:keys [file-not-found wrong-type load-exception]
                                           :or {wrong-type jobs/error-job
                                                file-not-found   jobs/error-job
                                                load-exception   jobs/error-job}}]
  (jobs/new-job ::load-file {:file file,
                             :transform acc-transform,
                             :wrong     wrong-type,
                             :not-found file-not-found
                             :exception load-exception}))

(defmethod jobs/do-job! ::load-file [job acc]
  (let [path       (-> job jobs/aux :file)
        transform  (-> job jobs/aux :transform)
        wrong-type (-> job jobs/aux :wrong)
        not-found  (-> job jobs/aux :not-found)
        exception  (-> job jobs/aux :exception)
        f          (path->file path)]
    (if-not (and (.exists f)
                 (.isFile f))
      (let [name (path->name path)
            job  (if (.exists f)
                   (wrong-type (str name " exists but is not a file, so it cannot be loaded.") {:name name, :file f,
                                                                                               :path path})
                   (not-found  (str name " doesnt exist, so it cannot be loaded.") {:name name, :file f,
                                                                                   :path path}))]
        (jobs/return acc [job]))
      (try
        (let [content (slurp f)]
          (jobs/return (transform acc content) []))
        (catch Exception e
          (let [name (path->name path)]
            (jobs/return acc [(exception (str "Encountered an unexpected exception while loading file '" name
                                              "' (" (.getMessage e) ").")
                                         {:name name,
                                          :file f,
                                          :path path,
                                          :exception e})])))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LOAD-RESOURCE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn load-resource-job [path acc-transform & {:keys [resource-not-found load-exception]
                                               :or   {resource-not-found jobs/error-job
                                                      load-exception     jobs/error-job}}]
  (jobs/new-job ::load-resource {:path path,
                                 :transform acc-transform,
                                 :not-found resource-not-found,
                                 :exception load-exception}))

(defmethod jobs/do-job! ::load-resource [job acc]
  (let [path      (-> job jobs/aux :path)
        transform (-> job jobs/aux :transform)
        not-found (-> job jobs/aux :not-found)
        exception (-> job jobs/aux :exception)
        resource-name (path->name path)
        ;; return nil if there is no such resource
        resource-file (jo/resource resource-name)]
    (if-not resource-file
      (jobs/return acc [(not-found (str "'" resource-name "' does not exist as a resource.") {:name resource-name,
                                                                                             :file resource-file,
                                                                                             :path path})])
      (try
        (let [content (slurp resource-file)]
          (jobs/return (transform acc content) []))
        (catch Exception e
          (jobs/return acc [(exception (str "We encountered an unexpected exception while trying to load the resource '"
                                            resource-name "' (" (.getMessage e) ").")
                                       {:name resource-name,
                                        :file resource-file,
                                        :path path,
                                        :exception e})]))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PATH->TREE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn- path->tree*-job [name path transform]
  (jobs/new-job ::path->tree* {:name name, :path path, :transform transform}))

(defmethod jobs/do-job! ::path->tree* [job acc]
  (let [name      (-> job jobs/aux :name)
        path      (-> job jobs/aux :path)
        transform (-> job jobs/aux :transform)
        file      (path->file path)
        ;; entries is `nil` if file is not a directory (or if it doesnt exist),
        ;; otherwise its an vector (even if the directory is empty!)
        entries   (.list file)
        jobs      (mapv (fn [name] (path->tree*-job name (add-to-path path [name]) #(merge %1 %2))) entries)]
    (if (nil? entries)
      ;; file is not a directory
      (jobs/return acc [(jobs/update-acc-job #(transform % {name :file}))])
      ;; file is a directory
      (jobs/return {} (into jobs
                            [(jobs/update-acc-job #(transform acc (hash-map name %)))])))))

(defn path->tree-job
  "If given a path to a file, return {:start :file}. Otherwise
  it will return the contents of that dir as a tree.

  EXAMPLE: If the path points to the following directory
  dir
  - subdir1
  -- file11
  -- file12
  - subdir2
  - file1

  it will return {:start {subdir1 {file11 :file, file12 :file}, subdir2 {}, file1 :file}}. "
  [path transform & {:keys [not-found]
                     :or   {not-found jobs/error-job}}]
  (jobs/new-job ::path->tree {:path path, :transform transform, :not-found not-found}))

(defmethod jobs/do-job! ::path->tree [job acc]
  (let [path      (-> job jobs/aux :path)
        not-found (-> job jobs/aux :not-found)
        transform (-> job jobs/aux :transform)]
    (jobs/return acc [(path-exists?-job path :not-found not-found)
                      (path->tree*-job :start path transform)])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LIST-FILES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn list-files-job [dir transform]
  (jobs/new-job ::list-files {:dir dir, :transform transform}))

(defmethod jobs/do-job! ::list-files [job acc]
  (let [dir (-> job jobs/aux :dir)
        transform (-> job jobs/aux :transform)]
    ;; TODO replace `:start` with something reasonable
    (jobs/return acc [(path->tree-job dir #(identity (:start %2)))
                      (jobs/update-acc-job #(transform acc (tree->paths %)))])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; CONFORM-ERROR
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn conform-error-job [string explain-data]
  (jobs/new-job ::conform-error {:string string,
                                 :data   explain-data}))

(defmethod jobs/do-job! ::conform-error [job acc]
  (let [explain-data (-> job jobs/aux :data)
        string       (-> job jobs/aux :string)
        problems     (with-out-str (spec/explain-printer explain-data))]
    (jobs/return acc [(jobs/error-job (str string "\n"
                                           problems))])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ZIP-FILES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; NEW-ZIP-ARCHIVE
(defn- new-zip-archive-job [zip-path transform]
  (jobs/new-job ::create-archive {:zip zip-path,
                                  :transform transform}))

(defmethod jobs/do-job! ::create-archive [job acc]
  (let [zip (-> job jobs/aux :zip)
        file (path->file zip)
        transform (-> job jobs/aux :transform)
        zip-out   (ZipOutputStream. (jo/output-stream file))]
    (jobs/return (transform acc zip-out) [])))

;;; ADD-ZIP-ENTRY
(defn- add-zip-entry-job [zip-out entry-name content]
  (jobs/new-job ::add-entry {:zip zip-out,
                             :name entry-name,
                             :content content}))

(defmethod jobs/do-job! ::add-entry [job acc]
  (let [zip (-> job jobs/aux :zip)
        name (-> job jobs/aux :name)
        content (-> job jobs/aux :content)
        data-bytes (.getBytes ^String (str content))
        data-len   (count data-bytes)]
    ;; TODO: add a catch block maybe ?
    (doto ^ZipOutputStream zip
      (.putNextEntry (ZipEntry. ^String (str name)))
      (.write        data-bytes 0 data-len)
      (.closeEntry))
    (jobs/return acc [])))

;;; FINALIZE-ZIP-ARCHIVE
(defn- finalize-zip-archive-job [zip-out]
  (jobs/new-job ::finalize-archive {:zip-out zip-out}))

(defmethod jobs/do-job! ::finalize-archive [job acc]
  (let [zip-out (-> job jobs/aux :zip-out)]
    (.close ^ZipOutputStream zip-out)
    (jobs/return acc [])))

;;; CREATE-ZIP
(defn create-zip-job [zip-path contents]
  (jobs/new-job ::create-zip {:zip zip-path,
                              :contents contents}))

(defmethod jobs/do-job! ::create-zip [job acc]
  (let [zip (-> job jobs/aux :zip)
        contents (-> job jobs/aux :contents)
        contents (reduce-kv (fn [acc path content] (assoc acc (path->name path) content)) {} contents)
        entry-creator (fn [entry content] (fn [zip-stream] (add-zip-entry-job zip-stream entry content)))
        entry-jobs (for [[entry content] contents] (jobs/instance-job (entry-creator entry content) identity))]
    (jobs/return acc (vec (concat [(new-zip-archive-job zip #(identity %2))]
                                  entry-jobs
                                  [(jobs/instance-job finalize-zip-archive-job identity)])))))

;;; ZIP-FILES
(defn zip-files-job
  "Creates a new zip archive at `zip-path`. `files-map` is a map path->path, where the key is the path inside the zip archive
  and the value is the path to the file whose contents are used."
  [zip-path files-map]
  (jobs/new-job ::zip-files {:files files-map,
                             :zip   zip-path}))

(defmethod jobs/do-job! ::zip-files [job acc]
  (let [files (-> job jobs/aux :files)
        zip   (-> job jobs/aux :zip)
        load-jobs (for [[k v] files] (load-file-job v #(assoc %1 k %2)))]
    (jobs/return {} (vec (concat load-jobs
                                 [(jobs/instance-job create-zip-job (jobs/already-instanced zip) identity)
                                  (jobs/restore-acc-job acc)])))))

