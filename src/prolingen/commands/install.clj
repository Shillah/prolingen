(ns prolingen.commands.install
  (:require [prolingen.io      :as io]
            [prolingen.jobs        :as jobs]
            
            [clojure.java.io       :as jo]
            [clojure.spec.alpha    :as s]
            [clojure.string        :as str]))

(s/def ::arguments empty?)

(defn conformed->arguments [conformed]
  conformed)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ENSURE-PATHS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def +PROLOG-FILES+ ["run.pl" "repl.pl" "librun.pl" "librepl.pl" "create-sav.pl" "libsav.pl"])

(defn- ensure-paths-jobs [info acc]
  [(io/create-paths-job io/prolingen-paths)])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PROLOG-FILES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn- prolog-files-jobs [info acc]
  (vec (for [name +PROLOG-FILES+]
         (let [resource (jo/resource name)
               contents (slurp resource)
               file     (io/add-to-path io/scripts-path [name] :file)]
           (io/new-file-job file contents)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SETTINGS-FILES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; GATHER-VENDORS

(defn gather-vendors-job [acc-transform]
  (jobs/new-job ::gather-vendors acc-transform))

(defmethod jobs/do-job! ::gather-vendors [job acc]
  (let [transform (-> job jobs/aux)]
    (jobs/return {} [(jobs/ask-question-job (str "Where is swipl installed ? (Press just ENTER to skip)")
                                            (fn [acc answer]
                                              (if (empty? answer)
                                                acc
                                                (assoc acc 'swipl {:location answer,
                                                                   :type     'swipl}))))
                     (jobs/ask-question-job (str "Where is sicstus installed ? (Press just ENTER to skip)")
                                            (fn [acc answer]
                                              (if (empty? answer)
                                                acc
                                                (assoc acc 'sicstus {:location answer,
                                                                     :type     'sicstus}))))
                     
                     (jobs/update-acc-job #(transform acc %))])))

;;; SELECT-DEFAULT-VENDOR

(defn select-default-vendor-job [vendors acc-transform]
  (jobs/new-job ::select-default {:vendors vendors,
                                  :transform acc-transform}))

(defmethod jobs/do-job! ::select-default [job acc]
  (let [vendors      (-> job jobs/aux :vendors)
        transform    (-> job jobs/aux :transform)
        vendor-names (->> vendors keys (mapv name))
        vendor-set   (set vendor-names)
        to-print     (str "Please select your default from among these by entering its name:\n> " (str/join "\n> " vendor-names)
                          "\n")
        ;; On bad input we warn the user and then restart.
        try-again    [(jobs/instance-job jobs/warning-job
                                         (jobs/already-instanced 2)
                                         #(str "'" % "' is not a valid input.")
                                         (jobs/already-instanced nil))
                      (jobs/insert-acc-job acc) job]
        accept       [(jobs/update-acc-job #(transform acc (symbol %)))]]
    (jobs/return acc [(jobs/ask-question-job to-print #(identity %2))
                      (jobs/if-job #(contains? vendor-set %) accept try-again)])))

;;; SETTINGS-FILES
(defn settings-files-jobs [info acc]
  [(jobs/insert-acc-job {})
   (gather-vendors-job #(assoc %1 :vendors %2))
   (jobs/if-job #(-> % :vendors not-empty)
                [(jobs/instance-job select-default-vendor-job :vendors (jobs/already-instanced #(assoc %1 :default-vendor %2)))]
                [(jobs/print-job "Dont forget to add :default-vendor to your settings file once you added at least one vendor!")])
   (jobs/update-acc-job #(assoc % :locations {}))
   (jobs/instance-job io/new-file-job (jobs/already-instanced io/settings-path) #(str %))
   (jobs/restore-acc-job acc)])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; CHECK-IF-EXISTS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def settings-root (let [home (java.lang.System/getProperty "user.home")]
                     (io/make-path :dir [home ".prol"])))

(defn check-if-exists-job [info acc]
  [(jobs/if-job (constantly (.exists (io/path->file settings-root))) [(jobs/error-job
                                                                       (str "prolingen is already installed!"))])])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; INSTALL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn- get-jobs [info acc]
  (concat (check-if-exists-job info acc)
          (ensure-paths-jobs info acc)
          (prolog-files-jobs info acc)
          (settings-files-jobs info acc)))

(defn install-job [info]
  (jobs/new-job ::install info))

(defmethod jobs/do-job! ::install [job acc]
  (let [info (jobs/aux job)
        jobs (get-jobs info acc)]
    (jobs/return acc jobs)))
