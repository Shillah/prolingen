(ns prolingen.commands.repl
  (:require [prolingen.io          :as io]
            [prolingen.vendor-api  :as vendor]

            [clojure.spec.alpha    :as s]))

(s/def ::arguments empty?)

(defn conformed->arguments [conformed]
  conformed)

(defn repl-job [info]
  (let [dep-paths (get info :dependency-paths)
        dep-names (mapv io/path->name dep-paths)
        src-path  (io/source-path info)
        src-dir   (io/path->file src-path)
        alias-map (merge (io/default-aliases info) (when dep-names {:library dep-names}))]
    ;;; TODO: each file should also have its module or "user"
    (vendor/repl-job info src-dir alias-map)))
