(ns prolingen.commands.build
  "The <build> command will execute swipl-ld/..."
  (:require #_[prolingen.command-api :as api]
            [prolingen.io      :as io]
            [prolingen.project     :as proj]
            [prolingen.jobs        :as jobs]
            [prolingen.tools.swipl-ld :as swipl-ld]
            [prolingen.tools.splfr    :as splfr]
            [prolingen.tools.spld    :as spld]
            
            [clojure.java.io       :as jo]
            [clojure.string        :as str]
            [clojure.spec.alpha    :as s]))

(def +SUPPORTED-SYSTEMS+ #{"swipl-ld",
                           "splfr",
                           "spld"})
(s/def ::supported-systems +SUPPORTED-SYSTEMS+)
(s/def ::arguments (s/cat :system ::supported-systems
                          :args   (s/+ string?)))

(defn conformed->arguments [conformed]
  (update conformed :system keyword))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; BUILD-JOB
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn build-job [info]
  (jobs/new-job ::build info))

(defmulti build-jobs-by-system (fn [acc arg info] (-> info :cmd :arguments :system)))

(defmethod build-jobs-by-system :default [acc _ info]
  (let [system (-> info :cmd :arguments :system)
        args    (-> info :cmd :arguments :args)]
    (if (or (nil? system) ; this could happen
            (= (name system) "")) ; this should never happen
      [(jobs/error-job (str "No built system was given."))]
      [(jobs/error-job (str "No built system '" (name system) "' is registered.") {:system system, :args args, :info info})])))

(defmethod jobs/do-job! ::build [job acc]
  (let [info (-> job jobs/aux)
        arguments (-> info :cmd :arguments :args)
        ;; arguments is a vec of strings, but we use symbols in the maps
        jobs      (vec (mapcat #(build-jobs-by-system acc (symbol %) info) arguments))]
    (jobs/return acc jobs)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SWIPL-LD
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn make-starter-file-job [path dependencies]
  (jobs/new-job ::starter-file {:path path, :deps dependencies}))

(defn- make-starter-string [deps]
  (str/join "\n" (mapcat (fn [[alias paths]] (map (fn [path]
                                                 (str ":- asserta(user:file_search_path(" (name alias) ", \"" path "\"))."))
                                               paths))
                       deps)))

(defmethod jobs/do-job! ::starter-file [job acc]
  (let [path  (-> job jobs/aux :path)
        deps  (-> job jobs/aux :deps)
        entry (-> job jobs/aux :entry)
        starter-string (make-starter-string deps) #_(if entry
                         (make-starter-string-with-entry deps entry)
                         (make-starter-string deps))]
    (jobs/return acc [(io/write-file-job path starter-string)])))

(defn get-swipl-ld-options [arg info]
  (let [swipl-ld (-> info :prj :swipl-ld)]
    (get swipl-ld arg)))

(defmethod build-jobs-by-system :swipl-ld [acc arg info]
  (let [swipl-ld-location (-> info :gbl :locations :swipl-ld)
        build-path        (io/build-path info)
        starter-path      (io/add-to-path build-path ["loader.pl"] :file)
        starter-name      [:prolingen.tools.swipl-ld/relative ['build "loader.pl"]]
        options'          (get-swipl-ld-options arg info)
        options           (update options' :pl-files #(into [starter-name] %))
        dep-paths         (get info :dependency-paths)
        dep-names         (mapv io/path->name dep-paths)
        prefixes          (io/default-aliases info)
        dependencies      (merge prefixes (when (not-empty dep-names) {:library dep-names}))]
    (if swipl-ld-location
      (if options'
        [(make-starter-file-job starter-path dependencies)
         (swipl-ld/swipl-ld-job prefixes swipl-ld-location options)]
        [(jobs/error-job (str "'" arg "' is not a valid swipl-ld option."))])
      [(jobs/error-job (str "':location :swipl-ld' was not set in your global and local options."))])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SPLFR
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn get-splfr-options [arg info]
  (let [splfr (-> info :prj :splfr)]
    (get splfr arg)))

(defmethod build-jobs-by-system :splfr [acc arg info]
  (let [splfr-location (-> info :gbl :locations :splfr)
        options           (get-splfr-options arg info)
        dep-paths (get info :dependency-paths)
        dep-names (mapv io/path->name dep-paths)
        prefixes (merge (io/default-aliases info) (when dep-names {:library dep-names}))]
    (cond
      (nil? splfr-location) [(jobs/error-job (str "':splfr-location' was not set in your global and local options."))]
      (nil? options)        [(jobs/error-job (str "'" arg "' is not the name of a splfr config."))]
      :else                 [(splfr/splfr-job splfr-location options prefixes)])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SPLD
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn get-spld-options [arg info]
  (let [spld (-> info :prj :spld)]
    (get spld arg)))

(defmethod build-jobs-by-system :spld [acc arg info]
  (let [spld-location (-> info :gbl :locations :spld)
        options           (get-spld-options arg info)]
    (cond
      (nil? spld-location) [(jobs/error-job (str "':spld-location' was not set in your global and local options."))]
      (nil? options)       [(jobs/error-job (str "'" arg "' is not the name of a spld config."))]
      :else [(spld/spld-job info spld-location options)])))
