(ns prolingen.commands.build-spec
  (:require [clojure.spec.alpha :as s]
            
            [prolingen.tools.swipl-ld :as swipl-ld]
            [prolingen.tools.splfr    :as splfr]
            [prolingen.tools.spld    :as spld]
            [prolingen.helper  :as h]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; BUILD-SPEC
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(s/def ::swipl-ld (s/map-of ::h/alpha-symbol ::swipl-ld/swipl-ld))
(s/def ::splfr (s/map-of ::h/alpha-symbol ::splfr/splfr))
(s/def ::spld (s/map-of ::h/alpha-symbol ::spld/spld))
