(ns prolingen.commands.hyperpl
  (:require [prolingen.jobs :as jobs]
            [prolingen.dependency :as dep]
            [prolingen.verify-dependencies :as deps]
            [prolingen.io :as io]

            [clojure.string :as str]
            [clojure.spec.alpha :as s]))

(s/def ::run-entry string?)
(s/def ::arguments (s/? ::run-entry))

(defn conformed->arguments [conformed]
  conformed)

;;; DEP-FILE-MAP
(defn- dep-file-map-job [dep transform]
  (jobs/new-job ::dep-map {:dep dep,
                           :transform transform}))

(defmethod jobs/do-job! ::dep-map [job acc]
  (let [dep (-> job jobs/aux :dep)
        transform (-> job jobs/aux :transform)
        dep-path (dep/dependency->path dep)
        dep-src  (io/add-to-path dep-path ["src"])]
    (jobs/return acc [(io/path->tree-job dep-src #(identity %2))
                      ;; get paths
                      (jobs/update-acc-job #(io/tree->paths % {:start "src"}))
                      ;; create map
                      (jobs/update-acc-job #(reduce (fn [acc path] (assoc acc path (io/merge-paths  dep-path path :file)))
                                                    {} %))
                      ;; "return"
                      (jobs/update-acc-job #(transform acc %))])))

;;; STARTER-FILE-MAP
(defn- load-deps [name deps]
  (let [src (io/make-path :dir ["src"])
        prj (io/make-path :dir ["src" name])]
    (str/join "\n" ["% -*- mode: prolog -*-"
                    (str ":- asserta(user:file_search_path(library, \"" (io/path->name src) "\")).")
                    (str ":- asserta(user:file_search_path(src, \"" (io/path->name prj) "\")).")])))
(defn- repl-pl   [name deps]
  (load-deps name deps))

(defn- remove-file-ending [f]
  (let [parts (str/split f #"\.")]
    (if (> (count parts) 1)
      (str/join "." (butlast parts))
      f)))

(defn- file->name
  "The `:file` entry in a run-entry is either just a string or a tuple of 2 strings.
  This helper function creates a single string from that."
  [file]
  (if (string? file)
    (remove-file-ending file)
    (let [alias (first file)
          file  (remove-file-ending (second file))]
      (str alias "(" file ")"))))

(defn- goal+module->goal [goal module]
  (if module
    (str module ":" goal)
    goal))

(defn- run-args [file goal]
  (str/join "\n" [(str ":- consult(" file "),")
                  "   current_prolog_flag(argv, Args),"
                  (str "   call(" goal ", Args),")
                  "   halt."]))

(defn- run-no-args [file goal]
  (str/join "\n" [(str ":- consult(" file "),")
                  (str "   call(" goal "),")
                  "   halt."]))

(defn- run [file goal args?]
  (if args?
    (run-args file goal)
    (run-no-args file goal)))

(defn- run-pl    [name deps run-entry]
  (let [loader (load-deps name deps)
        file   (:file   run-entry)
        real-file (file->name file)
        goal   (:goal   run-entry)
        args?  (:args?  run-entry)
        module (:module run-entry)
        real-goal (goal+module->goal goal module)
        run    (run real-file real-goal args?)]
    (str/join "\n" [loader run])))

(defn- starter-file-map-job [name run-entry deps transform]
  (let [repl-pl {(io/as-path :file "repl-starter.pl") (repl-pl name deps)}
        run-pl  (when run-entry {(io/as-path :file "starter.pl") (run-pl name deps run-entry)})
        map     (merge repl-pl run-pl)]
    (jobs/update-acc-job #(transform % map))))

;;; PROJECT-FILE-MAP
(defn- project-file-map-job [prj-path transform]
  (jobs/new-job ::project-map {:path prj-path,
                               :transform transform}))

(defmethod jobs/do-job! ::project-map [job acc]
  (let [prj-path (-> job jobs/aux :path)
        transform (-> job jobs/aux :transform)
        src-path (io/add-to-path prj-path ["src"])]
    (jobs/return acc [(io/path->tree-job src-path #(identity %2))
                      ;; get paths
                      (jobs/update-acc-job #(io/tree->paths % {:start "src"}))
                      ;; create map
                      (jobs/update-acc-job #(reduce (fn [acc path] (assoc acc path (io/merge-paths prj-path path :file)))
                                                    {} %))
                      ;; "return"
                      (jobs/update-acc-job #(transform acc %))])))

;;; LOAD-CONTENT
(defn load-content-job [file->file transform]
  (jobs/new-job ::load-content {:file->file file->file,
                                :transform transform}))

(defmethod jobs/do-job! ::load-content [job acc]
  (let [f->f (-> job jobs/aux :file->file)
        transform (-> job jobs/aux :transform)
        jobs (for [[k v] f->f] (io/load-file-job v #(assoc %1 k %2)))]
    (jobs/return {} (vec (concat jobs
                                 [(jobs/update-acc-job #(transform acc %))])))))

;;; CREATE-HYPERPL
(defn create-hyperpl-job [name zip-path prj-path deps run-entry]
  (jobs/new-job ::create-hyperpl {:path prj-path,
                                  :name name,
                                  :run-entry run-entry,
                                  :deps deps,
                                  :zip  zip-path}))

(defmethod jobs/do-job! ::create-hyperpl [job acc]
  (let [path (-> job jobs/aux :path)
        deps (-> job jobs/aux :deps)
        zip  (-> job jobs/aux :zip)
        name  (-> job jobs/aux :name)
        entry (-> job jobs/aux :run-entry)
        dep-file-map-jobs (map (fn [dep] (dep-file-map-job dep #(merge %1 %2))) deps)
        prj-file-map-job  (project-file-map-job path #(merge %1 %2))
        sta-file-map-job  (starter-file-map-job name entry deps #(merge %1 %2))]
    (jobs/return {} (vec (concat dep-file-map-jobs
                                 [prj-file-map-job
                                  (jobs/instance-job load-content-job identity (jobs/already-instanced #(identity %2)))
                                  :prolingen.jobs/halt
                                  sta-file-map-job
                                  (jobs/instance-job io/create-zip-job (jobs/already-instanced zip) identity)
                                  (jobs/restore-acc-job acc)])))))

(defn- arguments-error-job
  "Checks wether hyperpl was called in the right way. An appropriate error-job is returned on error;
  nil otherwise."
  [to-run run-entry]
  (when (if (not (empty? to-run)) ; if we got an argument but no run-entry, that means
          (nil? run-entry))       ; that the argument was wrong
    (jobs/error-job (str "'" to-run "' is not a valid run entry.") {:to-run to-run
                                                                    :entry run-entry})))

(defn hyperpl-job [info]
  (let [name (-> info :prj :name)
        zip-path (io/make-path :file ["build" (str name "-hyperpl.zip")])
        prefix   (-> info :flags :prefix)
        prj-path (if prefix
                   (io/as-path :dir prefix)
                   (io/as-path :dir "."))
        zip-path (io/merge-paths prj-path zip-path :file)
        deps     (-> info :all-deps)
        to-run   (-> info :cmd :arguments)
        run-entry (get (-> info :prj :run) to-run)]
    (if-let [error (arguments-error-job to-run run-entry)]
      error
      (create-hyperpl-job name zip-path prj-path deps run-entry))))

