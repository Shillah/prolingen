(ns prolingen.commands.test
  (:require [prolingen.io          :as io]
            [prolingen.vendor-api  :as vendor]
            [prolingen.jobs        :as jobs]

            [clojure.string        :as str]
            [clojure.spec.alpha    :as s]))

(s/def ::arguments    (s/+ string?))

(defn conformed->arguments [conformed]
  conformed)


(defn- as-goal [module goal args?]
  {:module module,
   :goal   goal,
   :args?  args?})

(defn test-job [info]
  (let [dep-paths (get info :dependency-paths)
        dep-names (mapv io/path->name dep-paths)
        default-aliases (io/default-aliases info)
        tests     (get-in info [:cmd :arguments])
        src-dir   (io/path->file (io/source-path info))
        alias-map (merge default-aliases (when (not-empty dep-names) {:library dep-names}))]
    ;;; TODO: each file should also have its module or "user"
    (jobs/new-job ::test {:src-dir src-dir, :info info, :tests tests
                          :aliases alias-map})))

(defn- get-run-job [info src-dir aliases data]
  (vendor/run-job info
                  src-dir
                  (dissoc data :file)
                  aliases
                  [(:file data)]
                  []))

(defn- get-jobs [info src-dir aliases [title data]]
  (if (nil? data)
    ;; We only use a warning here so that the other tests can continue to run
    [(jobs/warning-job 1 (str "There is no run goal with title '" title "'") {})]
    (let [c (count title)
          ;; 10 = %%%__ + __%%%
          title-length  (max c 50)
          border-length (+ 10 title-length)
          dif (- title-length c)
          border (clojure.string/join (repeat border-length "%"))
          filler (clojure.string/join (repeat dif "%"))]
      [(jobs/print-job (str "\n" border "\n" "%%%  " (clojure.string/upper-case title) "  %%%" filler "\n" border "\n"))
       (get-run-job info src-dir aliases data)])))

(defmethod jobs/do-job! ::test [job acc]
  (let [src-dir (-> job jobs/aux :src-dir)
        info    (-> job jobs/aux :info)
        tests   (-> job jobs/aux :tests)
        aliases (-> job jobs/aux :aliases)
        run-map    (get-in info [:prj :run])
        ;; there are two types of to-run entries:
        ;; first there is just the title
        ;; then there are sequences which contain the title and some additional arguments
        test-data  (map run-map tests)
        combined   (map vector tests test-data)
        jobs       (mapcat #(get-jobs info src-dir aliases %1) combined)]
    (jobs/return acc jobs)))
