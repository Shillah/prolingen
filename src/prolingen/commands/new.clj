(ns prolingen.commands.new
  "The <new> command will create a new Project"
  (:require [prolingen.io          :as io]
            [prolingen.project     :as proj]
            [prolingen.jobs        :as jobs]
            
            [clojure.java.io       :as jo]
            [clojure.string        :as str]
            [clojure.spec.alpha    :as s]))

(def +DEFINED-TEMPLATES+ #{"library" "program"})

(s/def ::template     +DEFINED-TEMPLATES+)
(s/def ::project-name string?)
(s/def ::vendor       string?)
(s/def ::arguments (s/cat :type ::template
                          :name ::project-name
                          :vendor (s/? ::vendor)))

(defn conformed->arguments [conformed]
  conformed)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MAKE-DIRECTORY-STRUCTURE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn make-directory-structure-job [info]
  (jobs/new-job ::make-directory info))

(defn project-tree [name]
  {name {"test"  :dir,
         "build" :dir,
         "src"   {name :dir}}})

(defmethod jobs/do-job! ::make-directory [job acc]
  (let [info  (-> job jobs/aux)
        name  (-> info :cmd :arguments :name)
        tree  (project-tree name)
        paths (io/tree->paths tree)
        job   (io/create-paths-job paths)]
    (jobs/return acc [job])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MAKE-PROJECT-FILE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmulti new-project (fn [info] (keyword (get-in info [:cmd :arguments :type]))))

(defmethod new-project :program [info]
  (let [cmd    (:cmd info)
        name   (-> cmd :arguments :name)
        vendor (-> info :gbl :default-vendor)]
    {:name name,
     :type :program
     :version "0.1.0",
     :license "NONE",
     :url (str "http://example.com/" name),
     :run {"main" {:file ["src" "main.pl"],
                   :goal "main",
                   :args? true,
                   :module "main"}
           "test-1" {:file ["test" "test.pl"]
                     :goal "test_me"
                     :args? false,
                     :module nil}}
     :vendors [vendor]
     :dependencies []}))

(defmethod new-project :library [info]
  (let [cmd (:cmd info)
        name (-> cmd :arguments :name)
        vendor (-> info :gbl :default-vendor)]
    {:name name,
     :type :library,
     :version "0.1.0",
     :license "NONE",
     :url (str "http://example.com/" name),
     :run {"test-1" {:file ["test" "test.pl"],
                     :goal "test_me"
                     :args? false,
                     :module nil}}
     :vendors      [vendor]
     :dependencies []}))

(defn make-project-job [info transform-acc]
  (jobs/new-job ::project {:info info,
                           :transform transform-acc}))

(defmethod jobs/do-job! ::project [job acc]
  (let [info (-> job jobs/aux :info)
        transform (-> job jobs/aux :transform)
        project   (new-project info)]
    (jobs/return (transform acc project) [])))

;;; MAKE-PROJECT-FILE

(defn make-project-file-job [info]
  (jobs/new-job ::project-file info))

(defn project-path [info]
  (let [name (-> info :cmd :arguments :name)
        root-path (io/as-dir-path name)
        prj-path  (io/add-to-path root-path ["project.clj"] :file)]
    prj-path))

(defmethod jobs/do-job! ::project-file [job acc]
  (let [info (-> job jobs/aux)
        project-path (project-path info)]
    (jobs/return acc [(make-project-job info #(identity %2))
                      (jobs/instance-job proj/project->string-job #(identity %) (jobs/already-instanced #(identity %2)))
                      (jobs/instance-job io/new-file-job (jobs/already-instanced project-path) #(identity %))
                      (jobs/restore-acc-job acc)])))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; GATHER-DATA
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn gather-data-job [info transform]
  (jobs/new-job ::gather-data {:info info, :transform transform}))

(defmulti data-job (fn [info transform] (-> info :cmd :arguments :type keyword)))

(defmethod data-job :default [info transform]
  (let [type (-> info :cmd :arguments :type)]
    (if type
      (jobs/error-job (str "'" (name type) "' is not a registered template name."))
      (jobs/error-job (str "No template name was supplied.")))))

(defmethod data-job :program [info transform]
  (let [name (-> info :cmd :arguments :name)]
    (jobs/update-acc-job #(transform % {:main-resource (io/make-path :file ["templates" "program" "main.pl"]),
                                        :main-path     (io/make-path :file [name "src" name "main.pl"]),
                                        :test-resource (io/make-path :file ["templates" "program" "test.pl"]),
                                        :test-path     (io/make-path :file [name "test" "test.pl"]),
                                        :placeholders  {:project-name name,
                                                        :lower-project-name (str/lower-case name)}}))))

(defmethod data-job :library [info transform]
  (let [name (-> info :cmd :arguments :name)]
    (jobs/update-acc-job #(transform % {:main-resource (io/make-path :file ["templates" "library" "library.pl"]),
                                        :main-path     (io/make-path :file [name "src" name (str name ".pl")]),
                                        :test-resource (io/make-path :file ["templates" "library" "test.pl"]),
                                        :test-path     (io/make-path :file [name "test" "test.pl"]),
                                        :placeholders  {:project-name name,
                                                        :lower-project-name (str/lower-case name)}}))))


(defmethod jobs/do-job! ::gather-data [job acc]
  (let [info (-> job jobs/aux :info)
        transform (-> job jobs/aux :transform)]
    (jobs/return acc [(data-job info #(transform acc %2))])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MAKE-RESOURCE-FILE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; REPLACE-PLACEHOLDERS

(defn- replace-placeholders-job [text placeholders acc-transform]
  (jobs/new-job ::replace-placeholders {:text text, :placeholders placeholders, :transform acc-transform}))

(defmethod jobs/do-job! ::replace-placeholders [job acc]
  (let [text         (-> job jobs/aux :text)
        transform    (-> job jobs/aux :transform)
        placeholders (-> job jobs/aux :placeholders)
        placeholders (reduce-kv (fn [acc k v] (let [new-k (->> k name (str "#") re-pattern)]
                                               (assoc acc new-k v))) {} placeholders)
        final-text   (reduce-kv (fn [acc k v] (str/replace acc k v)) text placeholders)]
    (jobs/return (transform acc final-text) [])))

;;; LOAD-RESOURCE-FILE

(defn load-resource-file-job [resource-path placeholders transform]
  (jobs/new-job ::load-resource {:resource      resource-path,
                                 :placeholders  placeholders,
                                 :transform     transform}))

(defmethod jobs/do-job! ::load-resource [job acc]
  (let [resource     (-> job jobs/aux :resource)
        placeholders (-> job jobs/aux :placeholders)
        transform    (-> job jobs/aux :transform)]
    (jobs/return acc [(io/load-resource-job resource #(identity %2))
                      (jobs/instance-job replace-placeholders-job #(identity %) (jobs/already-instanced placeholders)
                                         (jobs/already-instanced #(transform acc %2)))])))

;;; MAKE-RESOURCE-FILE

(defn make-resource-file-job [resource-path file-path placeholders]
  (jobs/new-job ::make-resource {:resource      resource-path,
                                 :file          file-path,
                                 :placeholders  placeholders}))

(defmethod jobs/do-job! ::make-resource [job acc]
  (let [resource     (-> job jobs/aux :resource)
        placeholders (-> job jobs/aux :placeholders)
        file    (-> job jobs/aux :file)]
    (jobs/return acc [(load-resource-file-job resource placeholders #(identity %2))
                      (jobs/instance-job io/new-file-job (jobs/already-instanced file) #(identity %))
                      (jobs/restore-acc-job acc)])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; NEW
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; ERROR-IF-DIR-EXISTS

(defn error-if-dir-exists-job [info]
  (jobs/new-job ::error info))

(defmethod jobs/do-job! ::error [job acc]
  (let [info (-> job jobs/aux)
        name (-> info :cmd :arguments :name)
        path (io/make-path :dir ["." name])
        file (io/path->file path)]
    (if (.exists file)
      (jobs/return acc [(jobs/error-job (str "'" (.getAbsolutePath file) "' already exists."))])
      (jobs/return acc []))))

;;; NEW

(defn new-job [info]
  (jobs/new-job ::new info))

(defmethod jobs/do-job! ::new [job acc]
  (let [info (-> job jobs/aux)]
    (jobs/return acc [(error-if-dir-exists-job      info)
                      (gather-data-job              info #(identity %2))
                      (make-directory-structure-job info)
                      (make-project-file-job        info)
                      (jobs/instance-job make-resource-file-job :main-resource :main-path :placeholders)
                      (jobs/instance-job make-resource-file-job :test-resource :test-path :placeholders)
                      (jobs/restore-acc-job         acc)])))
