(ns prolingen.commands.info
  (:require [prolingen.io          :as io]
            [prolingen.jobs        :as jobs]

            [clojure.spec.alpha    :as s]
            [clojure.string        :as str]))

(s/def ::arguments empty?)

(defn conformed->arguments [conformed]
  conformed)

(defmulti printer (fn [k prj] k))
(defmethod printer :default [k prj]
  ;; the default is to print nothing
  (jobs/print-job ""))
(defmethod printer :name [k prj]
  (jobs/print-job (str "project name:\n"
                       "> " (:name prj) "\n")))
(defmethod printer :version [k prj]
  (jobs/print-job (str "project version:\n"
                       "> " (:version prj) "\n")))
(defmethod printer :vendors [k prj]
  (let [vendors (:vendors prj)
        vendor-strs (mapv #(str "> " (name %) "\n") vendors)]
    (jobs/print-job (str "supported prolog vendors:\n"
                         (str/join vendor-strs)))))
(defmethod printer :run [k prj]
  (let [run-keys (keys (:run prj))
        run-strs (mapv #(str "> " % "\n") run-keys)]
    (jobs/print-job (str "run commands:\n"
                         (str/join run-strs)))))
(defmethod printer :dependencies [k prj]
  (let [deps     (:dependencies prj)
        dep-strs (mapv #(str "> " (:name %) " (from " (:origin %) ", version " (:version %) ")\n") deps)]
    (jobs/print-job (str "dependencies:\n"
                         (str/join dep-strs)))))

(defn info-job [info]
  (jobs/new-job ::info (:prj info)))

(defmethod jobs/do-job! ::info [job acc]
  (let [project (jobs/aux job)]
    (jobs/return acc [(jobs/print-job "Project info")
                      (jobs/print-job "+----------+")
                      (printer :name         project)
                      (printer :version      project)
                      (printer :vendors      project)
                      (printer :run          project)
                      (printer :dependencies project)])))
