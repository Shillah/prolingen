(ns prolingen.commands.run
  (:require [prolingen.io          :as io]
            [prolingen.vendor-api  :as vendor]
            [clojure.string        :as str]
            [prolingen.jobs        :as jobs]

            [clojure.spec.alpha    :as s]))

(s/def ::arguments (s/cat :to-run string?
                          :args   (s/* string?)))

(defn conformed->arguments [conformed]
  conformed)

(defn- as-goal [module goal args?]
  {:module module,
   :goal   goal,
   :args?  args?})

(defn run-job [info]
  (let [dep-paths (get info :dependency-paths)
        dep-names (mapv io/path->name dep-paths)
        default-aliases (io/default-aliases info)
        to-run    (get-in info [:cmd :arguments :to-run])
        args      (get-in info [:cmd :arguments :args])
        src-dir   (io/path->file (io/source-path info))
        alias-map (merge default-aliases (when (not-empty dep-names) {:library dep-names}))]
    ;;; TODO: each file should also have its module or "user"
    (jobs/new-job ::run {:src-dir src-dir, :info info, :to-run to-run, :arguments args
                         :aliases alias-map})))

(defn- get-run-job [info src-dir aliases args data]
  (vendor/run-job info
                  src-dir
                  (dissoc data :file)
                  aliases
                  [(:file data)]
                  args))

(defn- get-jobs [info src-dir aliases title data args]
  (if (nil? data)
    [(jobs/error-job (str "There is no run goal with title '" title "'") {})]
    [(get-run-job info src-dir aliases args data)]))

(defmethod jobs/do-job! ::run [job acc]
  (let [src-dir (-> job jobs/aux :src-dir)
        info    (-> job jobs/aux :info)
        to-run  (-> job jobs/aux :to-run)
        args    (-> job jobs/aux :arguments)
        aliases (-> job jobs/aux :aliases)
        run-map (get-in info [:prj :run])
        ;; there are two types of to-run entries:
        ;; first there is just the title
        ;; then there are sequences which contain the title and some additional arguments
        run-data   (run-map to-run)
        jobs       (get-jobs info src-dir aliases to-run run-data args)]
    (jobs/return acc jobs)))
