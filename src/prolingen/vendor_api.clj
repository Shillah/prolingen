(ns prolingen.vendor-api
  (:require [prolingen.jobs :as jobs]
            [prolingen.middleware :as middleware]
            [prolingen.io :as io]
            [prolingen.helper :as h]

            [clojure.spec.alpha :as s]))
(s/def ::location ::h/alpha-string)
(s/def ::type     ::h/alpha-symbol)
(s/def ::vendor (s/keys :req-un [::location ::type]))

(defn conformed->vendor-info [conformed]
  (update conformed :type keyword))

(defn vendor-info->conformed [conformed]
  (update conformed :type symbol))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; VENDOR-INFO
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn vendor-info-job [vendor info transform]
  (jobs/new-job ::vendor-info {:vendor vendor,
                               :info   info,
                               :transform transform}))

(defmethod jobs/do-job! ::vendor-info [job acc]
  (let [vendor (-> job jobs/aux :vendor)
        info   (-> job jobs/aux :info)
        transform (-> job jobs/aux :transform)
        vendors (get-in info [:gbl :vendors])]
    (if-let [vendor-info (get vendors vendor)]
      (jobs/return (transform acc vendor-info) [])
      (jobs/return acc [(jobs/error-job (str "'" (name vendor) "' is not a registered vendor."))]))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; RUN-OPTIONS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmulti run-options  (fn [vendor starter-file run-arguments] vendor))
(defmethod run-options :default [vendor _ _]
  nil)

(defn run-options-job [vendor-type starter run-args transform]
  (jobs/new-job ::run-options {:vendor vendor-type,
                               :starter starter,
                               :run-args run-args,
                               :transform transform}))

(defmethod jobs/do-job! ::run-options [job acc]
  (let [vendor (-> job jobs/aux :vendor)
        starter (-> job jobs/aux :starter)
        run-args (-> job jobs/aux :run-args)
        transform (-> job jobs/aux :transform)]
    (if-let [options (run-options vendor starter run-args)]
      (jobs/return (transform acc options) [])
      (jobs/return acc [(jobs/error-job (str "The prolog vendor type'" (name vendor) "' is not yet supported."))]))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; RUN
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn run-job [info src-dir goal aliases files arguments]
  (jobs/new-job ::run {:info    info,
                       :files   files
                       :src-dir src-dir
                       :args    arguments
                       :aliases aliases
                       :goal    goal}))

(defmethod jobs/do-job! ::run [job acc]
  (let [info      (-> job jobs/aux :info)
        files     (-> job jobs/aux :files)
        goal      (-> job jobs/aux :goal)
        args      (-> job jobs/aux :args)
        aliases   (-> job jobs/aux :aliases)
        src-dir   (-> job jobs/aux :src-dir)
        
        vendor    (get-in info [:prj :vendor])
        starter   (io/path->name io/run-file-path)
        run-args  (middleware/run-arguments goal aliases files args)]
    (jobs/return acc [(vendor-info-job vendor info #(identity %2))
                      ;; acc = {:type ..., :location ...}
                      (jobs/instance-job #(run-options-job %1 starter run-args
                                                           (fn [acc options] (assoc acc :options options))) :type)
                      (jobs/instance-job #(io/program-job %1 %2 :inherit-io true :redirect-error true :working-directory src-dir)
                                         :location :options)
                      (jobs/restore-acc-job acc)])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; REPL-OPTIONS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmulti repl-options (fn [vendor starter-script arguments] vendor))
(defmethod repl-options :default [vendor _ _]
  nil)

(defmulti repl-with-goal-options (fn [vendor starter-script arguments goal] vendor))
(defmethod repl-with-goal-options :default [vendor _ _]
  nil)

(defn repl-options-job [vendor-type starter repl-args goal transform]
  (jobs/new-job ::repl-options {:vendor vendor-type,
                               :starter starter,
                               :repl-args repl-args,
                               :transform transform,
                               :goal      goal}))

(defmethod jobs/do-job! ::repl-options [job acc]
  (let [vendor    (-> job jobs/aux :vendor)
        starter   (-> job jobs/aux :starter)
        repl-args (-> job jobs/aux :repl-args)
        transform (-> job jobs/aux :transform)
        goal      (-> job jobs/aux :goal)]
    (if-let [options (if goal
                       (repl-with-goal-options vendor starter repl-args goal)
                       (repl-options vendor starter repl-args))]
      (jobs/return (transform acc options) [])
      (jobs/return acc [(jobs/error-job (str "The prolog vendor type'" (name vendor) "' is not yet supported."))]))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; REPL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn repl-job
  ([info src-dir aliases] (jobs/new-job ::repl {:info info,
                                                :aliases aliases
                                                :src-dir src-dir}))
  ([info src-dir aliases goal] (jobs/new-job ::repl {:info info,
                                                     :aliases aliases
                                                     :src-dir src-dir,
                                                     :goal    goal})))

(defmethod jobs/do-job! ::repl [job acc]
  (let [info      (-> job jobs/aux :info)
        goal      (-> job jobs/aux :goal)
        args      (-> job jobs/aux :args)
        aliases   (-> job jobs/aux :aliases)
        
        vendor     (get-in info [:prj :vendor])
        starter    (io/path->name io/repl-file-path)
        repl-args  (middleware/repl-arguments aliases)]
    (jobs/return acc [(vendor-info-job vendor info #(identity %2))
                      ;; acc = {:type ..., :location ...}
                      (jobs/instance-job #(repl-options-job %1 starter repl-args goal
                                                            (fn [acc options] (assoc acc :options options))) :type)
                      (jobs/instance-job #(io/program-job %1 %2 :inherit-io true :redirect-error true) :location :options)
                      (jobs/restore-acc-job acc)])))
