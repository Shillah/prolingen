(ns prolingen.project
  "Reading and saving project meta data"
  (:require [clojure.java.io :as jo]
            [clojure.data.xml :as xml]
            [clojure.spec.alpha :as s]
            [clojure.pprint     :as print]
            
            [prolingen.io  :as io]
            [prolingen.jobs   :as jobs]
            [prolingen.dependency :as dep]
            [prolingen.prolog :as prol]
            [prolingen.helper :as h]

            [prolingen.commands.build-spec :as build]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PROJECT-SPEC
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(s/def ::vendors          (s/coll-of ::h/alpha-symbol))
(s/def ::dependency-entry (s/tuple   ::h/alpha-symbol ::h/alpha-string))
(s/def ::dependencies     (s/coll-of ::dependency-entry))

(s/def ::file             (s/or :no-alias   ::h/alpha-string
                                :with-alias (s/tuple ::h/alpha-symbol ::h/alpha-string)))
(s/def ::goal             ::h/alpha-symbol)
(s/def ::args?            boolean?)
(s/def ::run-entry        (s/keys :req-un [::file ::goal ::args?]))
(s/def ::run              (s/map-of ::h/alpha-symbol ::run-entry))

(s/def ::license          ::h/alpha-string)
(s/def ::url              ::h/alpha-string)
(s/def ::project-entries  (s/keys* :req-un [::vendors ::dependencies ::run]
                                   :opt-un [::license ::url
                                            ::build/swipl-ld
                                            ::build/splfr
                                            ::build/spld]))
(s/def ::project (s/cat :project? #{'defproject}
                        :name     ::h/alpha-symbol
                        :version  ::h/alpha-string
                        :entries  ::project-entries))

(defmulti conformed->entry (fn [k v] k))
(defmethod conformed->entry :default [k v]
  v)
(defmethod conformed->entry :vendors [k v]
  (mapv keyword v))
(defmethod conformed->entry :name   [k v]
  (name v))
(defmethod conformed->entry :type   [k v]
  (keyword v))
(defmethod conformed->entry :custom-path [k alias-map]
  (reduce-kv (fn [acc k v] (assoc acc (name k) v)) {} alias-map))
(defmethod conformed->entry :dependencies [k deps]
  (dep/unformed->dependency-entries deps))
(defn- stringify [path]
  (let [path-type (first path)
        path      (second path)]
    (case path-type
      :no-alias path
      :with-alias (mapv str path))))
(defmethod conformed->entry :run    [k v]
  (reduce-kv (fn [acc ke va] (assoc acc (str ke) (-> va
                                                    (update :file stringify )
                                                    (assoc  :module (namespace (:goal va)))
                                                    (update :goal name))))
             {} v))

(defn conformed->project [conformed]
  (as-> conformed %
    (dissoc % :project?)
    (merge (dissoc % :entries) (:entries %))
    (reduce-kv (fn [acc k v] (assoc acc k (conformed->entry k v))) {} %)
    (assoc % :vendor (first (:vendors %)))))

(defmulti entry->conformed (fn [k v] k))
(defmethod entry->conformed :default [k v]
  v)
(defmethod entry->conformed :vendors [k v]
  (mapv symbol v))
(defmethod entry->conformed :name   [k v]
  (symbol v))
(defmethod entry->conformed :type   [k v]
  (symbol v))
(defmethod entry->conformed :custom-path [k alias-map]
  (reduce-kv (fn [acc k v] (assoc acc (symbol k) v)) {} alias-map))
(defmethod entry->conformed :dependencies [k deps]
  (dep/dependency-entries->unformed deps))
(defmethod entry->conformed :run    [k v]
  (reduce-kv (fn [acc name data]
               (let [goal      (-> data :goal)
                     module    (-> data :module)
                     goal      (if module (str module "/" goal) goal)
                     goal      (symbol goal)
                     args?     (-> data :args?)
                     file      (-> data :file)
                     path      (if (string? file) file [(symbol (first file)) (second file)])
                     path-type (if (string? file) :no-alias :with-alias)]
                 (assoc acc (symbol name) {:file   [path-type path],
                                           :goal   goal,
                                           :args?  args?})))
             {}  v))

(defn project->conformed [project]
  (as-> project %
    (dissoc % :vendor)
    (reduce-kv (fn [acc k v] (assoc acc k (entry->conformed k v))) {} %)
    (assoc % :entries (-> % (dissoc :name) (dissoc :version)))
    (select-keys % [:name :version :entries])
    (assoc % :project? 'defproject)))

(defn- conformed->unformed [conformed]
  (->> conformed (s/unform ::project)))

(defn- unformed->conformed [unformed]
  (s/conform ::project unformed))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; FIND-MODULES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn find-modules-job [source-path transform]
  (jobs/new-job ::find-modules {:path source-path,
                                :transform transform}))

(defmethod jobs/do-job! ::find-modules [job acc]
  (let [path (-> job jobs/aux :path)
        transform (-> job jobs/aux :transform)]
    (jobs/return acc [(io/path->tree-job path #(identity %2))
                      (jobs/update-acc-job io/tree->paths)
                      ;; the paths have the form [:start "rest of path"]
                      ;; to fix them we need to remove :start from them
                      ;; and then merge them with path
                      (jobs/update-acc-job (fn [paths] (map (fn [p] (io/make-path (io/path-type p)
                                                                                (vec (rest (io/path-data p))))) paths)))
                      (jobs/update-acc-job (fn [paths] (map #(io/merge-paths path %) paths)))
                      (jobs/instance-job prol/module-declaration-from-paths-job
                                         identity
                                         (jobs/already-instanced #(transform acc %2)))])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LOAD-PROJECT
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn read-project-data-job [project-string transform-acc]
  (jobs/new-job ::read-project-data {:prj project-string,
                                     :transform transform-acc}))

(defmethod jobs/do-job! ::read-project-data [job acc]
  (let [project-string (-> job jobs/aux :prj)
        transform      (-> job jobs/aux :transform)]
    (try
      (let [project-data (read-string project-string)
            error        (s/explain-data ::project project-data)]
        ;; s/explain-data returns nil if there is no error (otherwise it returns a map!)
        (if error
          (jobs/return acc [(io/conform-error-job (str "An error occured while reading the project data:") error)])
          (let [conformed (s/conform ::project project-data)
                project   (conformed->project conformed)]
            (jobs/return (transform acc project) []))))
      (catch Exception e
        (jobs/return acc [(jobs/error-job (str "An exception occured while reading the project data: " (.getMessage e))
                                          {:exception e,
                                           :project-string project-string})])))))

(defn- load-project-file-job [file transform-acc]
  (jobs/new-job ::load-project-file {:file file,
                                     :transform transform-acc}))

(defmethod jobs/do-job! ::load-project-file [job acc]
  (let [path (-> job jobs/aux :file)
        transform (-> job jobs/aux :transform)]
    (jobs/return acc [(io/load-file-job path #(identity %2))
                      (jobs/instance-job read-project-data-job #(identity %1) (jobs/already-instanced #(transform acc %2)))])))

(defn load-project-job [flags transform-acc]
  (jobs/new-job ::load-project {:flags flags
                                :transform transform-acc}))

(defn- project-path [flags]
  (let [prefix (:prefix flags)
        with-prefix (if prefix #(vector prefix %) #(vector %))]
    (cond
      (:absolute-project-path flags) (io/make-path :file [(:absolute-project-path flags)])
      (:relative-project-path flags) (io/make-path :file (with-prefix (:relative-project-path flags)))
      :else                          (io/make-path :file (with-prefix "project.clj")))))

(defn- source-path [flags]
  (let [prefix (:prefix flags)
        with-prefix (if prefix #(vector prefix %) #(vector %))]
    (io/make-path :dir (with-prefix "src"))))

(defmethod jobs/do-job! ::load-project [job acc]
  (let [flags     (-> job jobs/aux :flags)
        path      (project-path flags)
        src-path  (source-path  flags)
        vendor    (keyword (:vendor flags))
        transform (-> job jobs/aux :transform)]
    (jobs/return acc [(jobs/if-job (fn [_] (.exists (io/path->file path)))
                                   [(load-project-file-job path #(identity %2))
                                    (jobs/update-acc-job #(transform acc (if vendor
                                                                           (assoc % :vendor vendor)
                                                                           %)))
                                    (find-modules-job src-path #(assoc % :modules %2))]
                                   [(jobs/error-job (str "The supposed project file '" (io/path->name path)
                                                         "' does not exist.") {:job job, :path path})])])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PROJECT->STRING
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn- header-str [name version]
  (str "(defproject " name " \"" version "\""))

(defmulti entry-str (fn [k v] k))
(defmethod entry-str :default [k v]
  (let [string (with-out-str
                 (print/pprint {k v}))
        len    (.length string)]
    ;; We remove the first character and the last two, since they are guaranteed
    ;; to be '{' and '}\n' respectively (which we dont want here!)
    (subs string 1 (- len 2))))
(defmethod entry-str :run [k run-entries]
  (let [unformed (s/unform ::run run-entries)
        string (with-out-str
                 (print/pprint {k unformed}))
        len    (.length string)]
    ;; We remove the first character and the last two, since they are guaranteed
    ;; to be '{' and '}\n' respectively (which we dont want here!)
    (subs string 1 (- len 2))))

(defmethod entry-str :swipl-ld [k swipl-ld]
  (let [unformed (s/unform ::build/swipl-ld swipl-ld)
        string (with-out-str
                 (print/pprint {k unformed}))
        len    (.length string)]
    ;; We remove the first character and the last two, since they are guaranteed
    ;; to be '{' and '}\n' respectively (which we dont want here!)
    (subs string 1 (- len 2))))

(defmethod entry-str :splfr [k splfr]
  (let [unformed (s/unform ::build/splfr splfr)
        string (with-out-str
                 (print/pprint {k unformed}))
        len    (.length string)]
    ;; We remove the first character and the last two, since they are guaranteed
    ;; to be '{' and '}\n' respectively (which we dont want here!)
    (subs string 1 (- len 2))))

(defmethod entry-str :spld [k spld]
  (let [unformed (s/unform ::build/spld spld)
        string (with-out-str
                 (print/pprint {k unformed}))
        len    (.length string)]
    ;; We remove the first character and the last two, since they are guaranteed
    ;; to be '{' and '}\n' respectively (which we dont want here!)
    (subs string 1 (- len 2))))


(defn- entries-str [entries]
  (let [strings (for [[k v] entries] (entry-str k v))]
    (as-> strings %
      (clojure.string/join "\n" %)
          ;;give them a small indent
      (clojure.string/replace % #"\n" "\n  "))))

(defn project->string [project]
  (let [conformed (-> project project->conformed)]
    (str (header-str (:name conformed) (:version conformed))
         "\n  "
         (entries-str (:entries conformed))
         ")")))

(defn project->string-job [project acc-transform]
  (jobs/new-job ::project->string {:project project,
                                   :transform acc-transform}))

(defmethod jobs/do-job! ::project->string [job acc]
  (let [project (-> job jobs/aux :project)
        transform (-> job jobs/aux :transform)]
    (jobs/return (transform acc (project->string project)) [])))
