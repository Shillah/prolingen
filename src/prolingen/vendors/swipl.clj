(ns prolingen.vendors.swipl
  (:require [prolingen.vendor-api :as api]))

(defmethod api/run-options :swipl [vendor starter-script arguments]
  (vec (concat ["--quiet"]
               ["-l" starter-script]
               #_["-g" "\"halt.\""]
               ["--"]
               arguments)))

(defmethod api/repl-options :swipl [vendor starter-script arguments]
  (vec (concat ["-l" starter-script]
               ["--"]
               arguments)))

(defmethod api/repl-with-goal-options :swipl [vendor starter-script arguments goal]
  (vec (concat ["-l" starter-script]
               ["-g" goal]
               ["--"]
               arguments)))
