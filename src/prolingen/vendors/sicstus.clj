(ns prolingen.vendors.sicstus
  (:require [prolingen.vendor-api :as api]))

(defmethod api/run-options :sicstus [vendor starter-script arguments]
  (vec (concat ["--nologo" "--noinfo"]
               ["-l" starter-script]
               #_["-g" "\"halt.\""]
               ["--"]
               arguments)))

(defmethod api/repl-options :sicstus [vendor starter-script arguments]
  (vec (concat ["-l" starter-script]
               ["--"]
               arguments)))

(defmethod api/repl-with-goal-options :sicstus [vendor starter-script arguments goal]
  (vec (concat ["-l" starter-script]
               ["--goal" goal]
               ["--"]
               arguments)))
