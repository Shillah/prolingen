(ns prolingen.helper
  (:require [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as gen]
            [clojure.string :as str]))

(s/def ::alpha-string (s/with-gen (s/and string?
                                         (complement str/blank?))
                        gen/string-alphanumeric))

;;; we append a so that the symbol is never confused with a number
(s/def ::alpha-symbol (s/with-gen symbol?
                        #(gen/fmap symbol
                                   (gen/fmap (fn [x] (str "a" x)) (s/gen ::alpha-string)))))
