(ns prolingen.tools.spld
  (:require [prolingen.io         :as io]
            [prolingen.jobs       :as jobs]
            [prolingen.middleware :as middleware]
            [prolingen.vendor-api :as vendor]
            [prolingen.helper :as h]

            [clojure.java.io          :as jo]
            [clojure.string           :as str]
            [clojure.spec.alpha :as s]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SPLD-OPTIONS-SPEC
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;; For all options & their explanation see:
;;; https://sicstus.sics.se/sicstus/docs/4.3.0/html/sicstus/too_002dspld.html#too_002dspld

(s/def ::spld-location ::h/alpha-string)
;;; flags
(s/def ::flag       #{:static :shared :verbose :vv :extended-rt :development :window :interactive :userhook
                      :memhook :no-locale :keep :nocompile :multi-sp-aware})
(s/def ::flags      (s/coll-of ::flag))
;;; almost flags (i.e. flags that have a yes-flag and a no-flag, e.g. --moveable/--no-moveable
(s/def ::moveable             boolean?)
(s/def ::resources-from-sav   boolean?)
(s/def ::embed-rt-sav         boolean?)
(s/def ::embed-sav-file       boolean?)
(s/def ::embed-license        boolean?)
;;; rest
(s/def ::alias     #{'src 'build 'test 'root})
(s/def ::filename   (s/or ::relative (s/tuple ::alias ::h/alpha-string)
                          ::absolute ::h/alpha-string))

(s/def ::main       #{:prolog :user :restore :load :none})
(s/def ::resources  (s/coll-of ::h/alpha-string))
(s/def ::respath    (s/coll-of ::h/alpha-string))
(s/def ::config     ::h/alpha-string)
(s/def ::conf       (s/map-of  ::h/alpha-string ::h/alpha-string))
(s/def ::cflag      (s/coll-of ::h/alpha-string))
(s/def ::LD         ::h/alpha-string)
(s/def ::sicstus    ::h/alpha-string)
(s/def ::locale     ::h/alpha-string)
(s/def ::with-jdk   ::h/alpha-string)
(s/def ::with-tcltk ::h/alpha-string)
(s/def ::with-tk    ::h/alpha-string)
(s/def ::with-bdb   ::h/alpha-string)
(s/def ::with-tcl   ::h/alpha-string)
(s/def ::namebase   ::h/alpha-string)
(s/def ::license-file ::h/alpha-string)

(s/def ::inputs     (s/coll-of ::filename))
(s/def ::output     ::filename)
(s/def ::create-sav (s/keys :req-un [::inputs ::output]))

(s/def ::spld (s/keys :opt-un [::flags ::moveable ::resources-from-sav ::embed-rt-sav ::embed-sav-file ::embed-license
                                ::main ::resources ::respath ::config ::conf ::cflag ::LD ::sicstus ::locale ::output ::namebase
                                ::with-jdk ::with-tcltk ::with-tk ::with-tcl ::with-bdb ::license-file ::create-sav]))

(defmulti conformed->entry (fn [info k v] k))
(defmethod conformed->entry :default [info k v]
  v)
(defn- create-sav-stringify [x]
  (if (= (first x) ::absolute)
    (str "'" (second x) "'")
    (let [tuple (second x)
          alias (str (first tuple))
          string (second tuple)]
      (vector alias string))))
(defmethod conformed->entry :create-sav [info k v]
  (-> v
      (update :inputs #(mapv create-sav-stringify %))
      (update :output create-sav-stringify)))
(defmethod conformed->entry :output     [info k v]
  (let [t (first v)
        v (second v)]
    (if (= t ::absolute)
      v
      (let [alias (first v)
            string (second v)
            aliases (io/default-aliases info)
            prefix  (first (get aliases (keyword alias)))
            complete-path (io/make-path :file [prefix string])]
        (io/path->name complete-path)))))

(defn conformed->options [info conformed]
  (reduce-kv (fn [acc k v] (assoc acc k (conformed->entry info k v))) {} conformed))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SPLD-SPEC
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmulti option-string
  "Turns the option into a (spld readable) list of strings."
  (fn [option val] option))

(defmulti flag-name identity)
(defmethod flag-name :default [flag]
  (str "--" (name flag)))
(defmethod flag-name :vv [flag]
  "-vv")

(defmethod option-string :flags [option v]
  (mapv flag-name v))

(defmethod option-string :moveable [option v]
  (if v
    ["--moveable"]
    ["--no-moveable"]))

(defmethod option-string :resources-from-sav [option v]
  (if v
    ["--resources-from-sav"]
    ["--no-resources-from-sav"]))

(defmethod option-string :embed-rt-sav [option v]
  (if v
    ["--embed-rt-sav"]
    ["--no-embed-rt-sav"]))

(defmethod option-string :embed-sav-file [option v]
  (if v
    ["--embed-sav-file"]
    ["--no-embed-sav-file"]))

(defmethod option-string :embed-license [option v]
  (if v
    ["--embed-license"]
    ["--no-embed-license"]))

(defmethod option-string :output [option v]
  [(str "--output=" v)])

(defmethod option-string :main [option v]
  [(str "--main=" (name v))])

(defmethod option-string :resources [option resource-list]
  [(str "--resources=" (str/join "," resource-list))])

;;; for some reason, its respath (and only it!) is semicolon seperated on windows
;;; (comma-seperated otherwise).
(defmethod option-string :respath [option path-list]
  [(str "--respath=" (str/join (if (io/is-windows?) ";" ",") path-list))])

(defmethod option-string :config [option v]
  [(str "--config=" v)])

(defmethod option-string :conf [option conf-map]
  (reduce-kv (fn [acc k v] (into acc ["--conf" (str k "=" v)])) [] conf-map))

(defmethod option-string :cflag [option v]
  (mapv (fn [flag] (str "--cflag=" flag)) v))

(defmethod option-string :LD [option v]
  ["--LD" v])

(defmethod option-string :sicstus [option v]
  [(str "--sicstus=" v)])

(defmethod option-string :locale [option v]
  [(str "--locale=" v)])

(defmethod option-string :with-jdk [option v]
  [(str "--with_jdk=" v)])

(defmethod option-string :with-tcltk [option v]
  [(str "--with_tcltk=" v)])

(defmethod option-string :with-tcl [option v]
  [(str "--with_tcl=" v)])

(defmethod option-string :with-tk [option v]
  [(str "--with_tk=" v)])

(defmethod option-string :with-bdb [option v]
  [(str "--with_bdb=" v)])

(defmethod option-string :namebase [option v]
  [(str "--namebase=" v)])

(defmethod option-string :output [option v]
  [(str "--output=" v)])

(defmethod option-string :license-file [option v]
  [(str "--license-file=" v)])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; CREATE-SAV
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defn compile-string [inputs output]
  (let [;input-strings (mapv (fn [input] (str "compile(" input ")")) inputs)
        input-string (vec (interpose "-" inputs))]
    (vec (flatten (into input-string ["--" output "--"])))))

(defn create-sav-job [info create-sav]
  (jobs/new-job ::create-sav {:info info,
                              :create-sav create-sav}))

(defmethod jobs/do-job! ::create-sav [job acc]
  (let [info (-> job jobs/aux :info)
        create-sav (-> job jobs/aux :create-sav)]
    (if create-sav (let [dep-paths (get info :dependency-paths)
                         dep-names (mapv io/path->name dep-paths)
                         src-path  (io/source-path info)
                         src-dir   (io/path->file src-path)
                         alias-map (merge (io/default-aliases info) (when dep-names {:library dep-names}))
                         io-str    (compile-string (:inputs create-sav) (:output create-sav))
                         sicstus-path (get-in info [:gbl :vendors :sicstus :location])
                         repl-args (middleware/repl-arguments alias-map)
                         repl-args-with-io (into io-str repl-args)
                         starter   (io/path->name io/create-sav-file-path)
                         options   (vendor/repl-options :sicstus starter repl-args-with-io)]
                     (cond
                       (nil? sicstus-path) (jobs/return acc [(jobs/error-job (str "':sicstus' path is not set.") {:info info})])
                       (nil? (:inputs create-sav)) (jobs/return acc [(jobs/error-job (str "No inputs specified for 'create-sav'")
                                                                                     {:info info})])
                       :else (jobs/return acc [(io/program-job sicstus-path options :inherit-io true :redirect-error true)]))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SPLD
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn option-strings
  "Returns a list of strings that contains all options in a way that `program-job` understands them.
  Its important that the '--LD' option comes last, since that option tells splfr to ignore the following arguments and
  give them verbatim to the compiler/linker."
  [options]
  (if (:LD options)
    (let [ld (:LD options)
          options (dissoc options :LD)
          strings (vec (mapcat (fn [[k v]] (option-string k v)) options))
          ld-string (option-string :LD ld)]
      ;; this will put ld-strings at the back
      ;; since strings is guaranteed to be a vector.
      (into strings ld-string))
    (vec (mapcat (fn [[k v]] (option-string k v)) options))))

(defn input-string [input info]
  ;; input is a tuple (::path-with/without-alias <actual-input>)
  ;; and input is either just a string or a tuple [alias string]
  (let [type (first input)
        path (second input)
        aliases (io/default-aliases info)]
    (if (= type ::path-without-alias)
      path
      (let [alias (first path)
            string (second path)
            prefix-path (first (get aliases (keyword alias)))
            final-path (io/add-to-path prefix-path [string] :file)]
        (io/path->name final-path)))))

(defn input-strings [inputs info]
  (mapv #(input-string % info) inputs))

(defn program-args [options input]
  (vec (concat input options)))

(defn spld-job [info spld-location options]
  (let [input   (:input options)
        options (dissoc options :input)]
    (jobs/new-job ::spld {:info     info
                          :location spld-location,
                          :input input,
                          :options options})))


(defmethod jobs/do-job! ::spld [job acc]
  (let [options (-> job jobs/aux :options)
        info    (-> job jobs/aux :info)
        options (conformed->options info options)
        input   (-> job jobs/aux :input)
        location (-> job jobs/aux :location)

        create-sav     (:create-sav options)
        options        (dissoc options :create-sav)
        option-strings (option-strings options)
        input-strings  (input-strings input info)
        program-args (program-args option-strings input-strings)]
    (jobs/return acc [(create-sav-job info create-sav)
                      (io/program-job location program-args)])))
