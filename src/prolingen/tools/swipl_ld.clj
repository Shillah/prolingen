(ns prolingen.tools.swipl-ld
  (:require [prolingen.io             :as io]
            [prolingen.jobs           :as jobs]
            [prolingen.helper         :as h]

            [clojure.java.io          :as jo]
            [clojure.string           :as str]
            [clojure.spec.alpha :as s]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SWIPL-LD-OPTIONS-SPEC
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(s/def ::swipl-ld-location ::h/alpha-string)

(s/def ::alias     #{'src 'build 'test 'root})
(s/def ::filename   (s/or ::relative (s/tuple ::alias ::h/alpha-string)
                          ::absolute ::h/alpha-string))

(s/def ::flag       #{:help :nostate :shared :dll :c :E :v})
(s/def ::flags      (s/coll-of ::flag))
(s/def ::goal       boolean?)
(s/def ::pl         ::h/alpha-string)
(s/def ::ld         ::h/alpha-string)
(s/def ::cc         ::h/alpha-string)
(s/def ::c++        ::h/alpha-string)
(s/def ::pl-option  ::h/alpha-string)
(s/def ::pl-options (s/coll-of ::pl-option))
(s/def ::ld-option  ::h/alpha-string)
(s/def ::ld-options (s/coll-of ::pl-option))
(s/def ::cc-option  ::h/alpha-string)
(s/def ::cc-options (s/coll-of ::pl-option))
(s/def ::output     ::filename)
(s/def ::library    ::h/alpha-string)
(s/def ::l          (s/coll-of ::library))
(s/def ::library-dir ::h/alpha-string)
(s/def ::L          (s/coll-of ::library-dir))
(s/def ::include-dir  ::h/alpha-string)
(s/def ::I          (s/coll-of ::include-dir))
(s/def ::def ::h/alpha-string)
(s/def ::D          (s/coll-of ::def))
(s/def ::c-files    (s/coll-of ::filename))
(s/def ::pl-files   (s/coll-of ::filename))
;;; what is -g supposed to do ?
(s/def ::swipl-ld (s/keys :opt-un [::flags ::pl ::ld ::cc ::c++ ::pl-options ::ld-options ::cc-options
                                    ::l ::L ::I ::D ::goal]
                           :req-un [::c-files ::pl-files ::output]))

(defn filename->string [prefixes filename]
  (let [t (first filename) ; ::absolute or ::relative
        v (second filename)]
    (if (= t ::absolute)
      v
      (let [alias (first v)
            string (second v)
            ;; prefixes are stored in a keyword -> string map
            prefix  (first (get prefixes (keyword alias)))
            complete-path (io/make-path :file [prefix string])]
        (io/path->name complete-path)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SWIPL-LD
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmulti option-string
  "Turns the option into a (swipl-ld readable) list of strings."
  (fn [prefixes option val] option))

(defmethod option-string :flags [prefixes option v]
  (mapv #(->> % name (str "-")) v))

(defmethod option-string :pl [prefixes option v]
  ["-pl" v])

(defmethod option-string :ld [prefixes option v]
  ["-ld" v])

(defmethod option-string :cc [prefixes option v]
  ["-cc" v])

(defmethod option-string :c++ [prefixes option v]
  ["-c++" v])

(defmethod option-string :pl-options [prefixes option v]
  (let [new-v (str/join "," v)]
    [(str "-pl-options," new-v)]))

(defmethod option-string :ld-options [prefixes option v]
  (let [new-v (str/join "," v)]
    [(str "-ld-options," new-v)]))

(defmethod option-string :cc-options [prefixes option v]
  (let [new-v (str/join "," v)]
    [(str "-cc-options," new-v)]))

(defmethod option-string :output [prefixes option v]
  (let [string (filename->string prefixes v)]
    ["-o" string]))

(defmethod option-string :l [prefixes option v]
  (mapv #(str "-l" %) v))

(defmethod option-string :L [prefixes option v]
  (mapv #(str "-L" %) v))

(defmethod option-string :I [prefixes option v]
  (mapv #(str "-I" %) v))

(defmethod option-string :D [prefixes option v]
  (mapv #(str "-D" %) v))

(defmethod option-string :goal [prefixes option v]
  ["-goal" v])

(defn option-strings
  "Returns a list of strings that contains all options in a way that `program-job` understands them."
  [prefixes options]
  (vec (mapcat (fn [[k v]] (option-string prefixes k v)) options)))

(defn program-args [options c-files pl-files]
  (vec (concat options c-files pl-files)))

(defn swipl-ld-job [prefixes swipl-ld-location options]
  (let [c-files (:c-files options)
        pl-files (:pl-files options)
        options (dissoc options :c-files :pl-files)]
    (jobs/new-job ::swipl-ld {:location swipl-ld-location,
                              :c-files c-files,
                              :pl-files pl-files,
                              :options options,
                              :prefixes    prefixes})))


(defmethod jobs/do-job! ::swipl-ld [job acc]
  (let [options (-> job jobs/aux :options)
        c-files (-> job jobs/aux :c-files)
        pl-files (-> job jobs/aux :pl-files)
        location (-> job jobs/aux :location)
        prefixes     (-> job jobs/aux :prefixes)
        option-strings (option-strings prefixes options)
        c-strings    (mapv #(filename->string prefixes %) c-files)
        pl-strings    (mapv #(filename->string prefixes %) pl-files)
        program-args (program-args option-strings c-strings pl-strings)]
    (jobs/return acc [(io/program-job location program-args)])))
