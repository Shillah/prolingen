(ns prolingen.tools.splfr
  (:require [prolingen.io             :as io]
            [prolingen.jobs           :as jobs]
            [prolingen.helper         :as h]

            [clojure.java.io          :as jo]
            [clojure.string           :as str]
            [clojure.spec.alpha :as s]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SPLFR-OPTIONS-SPEC
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; For all options & their explanation see:
;;; https://sicstus.sics.se/sicstus/docs/4.3.0/html/sicstus/too_002dsplfr.html#too_002dsplfr

(s/def ::splfr-location ::h/alpha-string)

(s/def ::alias     #{'src 'build 'test 'root})
(s/def ::filename   (s/or ::relative (s/tuple ::alias ::h/alpha-string)
                          ::absolute ::h/alpha-string))

(s/def ::flag       #{:verbose :vv :keep :static :no-rpath :nocompile :multi-sp-aware :structs :objects})
(s/def ::flags      (s/coll-of ::flag))
(s/def ::config     ::h/alpha-string)
(s/def ::conf       (s/map-of  ::h/alpha-string ::h/alpha-string))
(s/def ::cflag      (s/coll-of ::h/alpha-string))
(s/def ::LD         ::h/alpha-string)
(s/def ::sicstus    ::h/alpha-string)
(s/def ::resource   ::h/alpha-string)
(s/def ::output     ::filename)
(s/def ::namebase   ::h/alpha-string)
(s/def ::header     ::h/alpha-string)
(s/def ::moveable   boolean?)
(s/def ::input      (s/coll-of ::filename))

;;; what is -g supposed to do ?
(s/def ::splfr (s/keys :opt-un [::flags ::config ::conf ::cflag ::LD ::sicstus ::resource ::output ::namebase ::header ::moveable]
                        :req-un [::input]))

(defn filename->string [prefixes filename]
  ;; is a tuple [absolute/relative s]
  ;; where s is a string if its absolute
  ;; and another tuple [prefix rest] if its relative
  (let [type (first filename)
        s    (second filename)]
    (if (= ::absolute type)
      s
      (let [prefix (first s)
            rest   (second s)
            prefix-string (first (prefixes (keyword prefix)))]
        (io/path->name (io/make-path :file [prefix-string rest]))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SPLFR
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmulti option-string
  "Turns the option into a (splfr readable) list of strings."
  (fn [prefixes option val] option))

(defmulti flag-name identity)
(defmethod flag-name :default [flag]
  (str "--" (name flag)))
(defmethod flag-name :vv [flag]
  "-vv")

(defmethod option-string :flags [prefixes option v]
  (mapv flag-name v))

(defmethod option-string :config [prefixes option v]
  [(str "--config=" v)])

(defmethod option-string :conf [prefixes option conf-map]
  (reduce-kv (fn [acc k v] (into acc ["--conf" (str k "=" v)])) [] conf-map))

(defmethod option-string :cflag [prefixes option v]
  (mapv (fn [flag] (str "--cflag=" flag)) v))

(defmethod option-string :LD [prefixes option v]
  ["--LD" v])

(defmethod option-string :sicstus [prefixes option v]
  [(str "--sicstus=" v)])

(defmethod option-string :resource [prefixes option v]
  [(str "--resource=" v)])

(defmethod option-string :namebase [prefixes option v]
  [(str "--namebase=" v)])

(defmethod option-string :output [prefixes option v]
  [(str "--output=" (filename->string prefixes v))])

(defmethod option-string :moveable [option v]
  (if v
    ["--moveable"]
    ["--no-moveable"]))

(defn option-strings
  "Returns a list of strings that contains all options in a way that `program-job` understands them.
  Its important that the '--LD' option comes last, since that option tells splfr to ignore the following arguments and
  give them verbatim to the compiler/linker."
  [prefixes options]
  (if (:LD options)
    (let [ld (:LD options)
          options (dissoc options :LD)
          strings (vec (mapcat (fn [[k v]] (option-string prefixes k v)) options))
          ld-string (option-string :LD ld)]
      ;; this will put ld-strings at the back
      ;; since strings is guaranteed to be a vector.
      (into strings ld-string))
    (vec (mapcat (fn [[k v]] (option-string prefixes k v)) options))))

(defn program-args [options input]
  (vec (concat input options)))

(defn splfr-job [splfr-location options prefixes]
  (let [input   (:input options)
        options (dissoc options :input)]
    (jobs/new-job ::splfr {:location splfr-location,
                           :input input,
                           :options options,
                           :prefixes prefixes})))


(defmethod jobs/do-job! ::splfr [job acc]
  (let [options (-> job jobs/aux :options)
        input   (-> job jobs/aux :input)
        location (-> job jobs/aux :location)
        prefixes (-> job jobs/aux :prefixes)
        option-strings (option-strings prefixes options)
        program-args (program-args option-strings (mapv #(filename->string prefixes %) input))]
    (jobs/return acc [(io/program-job location program-args)])))
