(ns prolingen.create-dependency
  (:require [prolingen.io :as io]
            [prolingen.jobs :as jobs]
            [prolingen.project :as proj]
            [prolingen.dependency :as dep]
            [prolingen.prolog :as prol]

            [clojure.string :as str]
            [clojure.spec.alpha :as s]))

;;; These are the files that get copied/loaded
(def +IMPORTANT-FILES+ ["src" "project.clj"])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PROJECT->DEPENDENCY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; TRANSFORM-FILES
(defn transform-files-job [paths transform]
  (jobs/new-job ::transform-files {:paths paths, :transform transform}))

(defn- remove-src [path]
  (let [type (io/path-type path)
        data (io/path-data path)
        new-data (rest (drop-while #(not (= "src" %)) data))]
    ;; if src wasnt in data, we return the old path
    (if new-data
      (io/make-path type (vec new-data))
      path)))
(defmethod jobs/do-job! ::transform-files [job acc]
  (let [paths (-> job jobs/aux :paths)
        transform (-> job jobs/aux :transform)
        files (vec (->> paths (map remove-src) (map io/path->name)))]
    (jobs/return (transform acc files) [])))

;;; PROJECT->DEPENDENCY
(defn- project->dependency*-job
  [dep-path prj-path version important-files]
  (jobs/new-job ::prj-dependency* {:dep dep-path, :prj prj-path, :files important-files, :version version}))

(defmethod jobs/do-job! ::prj-dependency* [job acc]
  ;; We need to do 2 things here:
  ;; 1) copy src from prj to dep
  ;; 2) create a dependency.clj file
  (let [dep   (-> job jobs/aux :dep)
        prj   (-> job jobs/aux :prj)
        files (-> job jobs/aux :files)
        real-version (-> job jobs/aux :version)
        dep-name (io/path->name dep)
        prj-name (io/path->name prj)
        from-prj #(io/merge-paths prj %)
        to-dep   #(io/merge-paths dep %)
        src   (get files "src")
        prj   (get files "project.clj")
        ;; 1) ################################################
        load-jobs  (map (fn [path] (io/load-file-job (from-prj path) #(assoc %1 (from-prj path) %2))) src)
        write-jobs (map (fn [path] (jobs/instance-job io/new-file-job
                                                     (jobs/already-instanced (to-dep path))
                                                     #(get % (from-prj path)))) src)
        ;; ###################################################


        ;; 2) ################################################
        ;; to create the dependency.clj, we need to find all modules & all files
        ;; that are used.
        ;; Thankfully the accumulator will be exactly the map of all files
        ;; to their contents, so both of these are easy to find out
        dependency-path (io/add-to-path dep ["dependency.clj"] :file)

        ;; split the map path->content into a list of paths & contents
        ;; we find all files by using the paths (duh!)
        ;; and all modules by analyzing their contents.
        ;; For that we search for ':- module(name)' declarations
        seperator-job (jobs/update-acc-job #(hash-map :paths (keys %) :contents (vals %)))
        files-job     (jobs/instance-job transform-files-job :paths (jobs/already-instanced #(assoc %1 :files %2)))
        modules-job   (jobs/instance-job prol/module-declaration-from-paths-job :paths
                                         (jobs/already-instanced #(assoc %1 :modules %2)))
        load-project-job      (proj/load-project-job {:prefix prj-name} #(assoc %1 :proj %2))
        create-dependency-job (jobs/update-acc-job (fn [acc] (let [n       (-> acc :proj :name)
                                                                  version real-version #_(-> acc :proj :version)
                                                                  vendors (-> acc :proj :vendors)
                                                                  deps    (-> acc :proj :dependencies)
                                                                  mods    (-> acc :modules)
                                                                  files   (-> acc :files)
                                                                  dep     {:name n,
                                                                           :version version,
                                                                           :vendors vendors
                                                                           :dependencies deps,
                                                                           :modules      (mapv symbol mods),
                                                                           :files        files}]
                                                              (dep/dependency->string dep))))
        write-dependency-job  (jobs/instance-job io/new-file-job (jobs/already-instanced dependency-path) identity)]
    (jobs/return {} (vec (concat load-jobs
                                 write-jobs
                                 [seperator-job
                                  files-job
                                  modules-job
                                  load-project-job
                                  create-dependency-job
                                  write-dependency-job
                                  (jobs/restore-acc-job acc)])))))

(defn project->dependency-job
  "`project-path` is where the project is and `dep-path` is where the dependency should be."
  [dep-path project-path real-version]
  (jobs/new-job ::prj-dependency {:dep-path dep-path,
                                  :prj-path project-path,
                                  :version real-version}))

(defmethod jobs/do-job! ::prj-dependency [job acc]
  (let [dep-path (-> job jobs/aux :dep-path)
        prj-path (-> job jobs/aux :prj-path)
        version  (-> job jobs/aux :version)]
    (jobs/return acc [(io/path->tree-job prj-path #(identity %2))
                      (jobs/update-acc-job (fn [tree] (reduce (fn [acc k]
                                                               (let [key-tree {k (get-in tree [:start k])}]
                                                                 (assoc acc k (io/tree->paths key-tree))))
                                                             {} +IMPORTANT-FILES+)))
                      (jobs/instance-job project->dependency*-job
                                         (jobs/already-instanced dep-path)
                                         (jobs/already-instanced prj-path)
                                         (jobs/already-instanced version)
                                         identity)])))
