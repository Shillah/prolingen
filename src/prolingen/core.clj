(ns prolingen.core
  (:require [prolingen.command-api    :as api]
            [prolingen.project        :as proj]
            [prolingen.io             :as io]
            [prolingen.jobs           :as jobs]

            ;; We require the default supported vendors here
            ;; so that they get loaded on startup
            prolingen.vendors.swipl
            prolingen.vendors.sicstus

            [clojure.java.io          :as jo]
            [clojure.spec.alpha       :as s]
            clojure.set)
  (:gen-class))

(def +NAME+ "prolingen")
(def +VERSION+ "0.1.0")

(def print-banner-job (jobs/print-job (str "Starting " +NAME+ " " +VERSION+ ".\n\n\n")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; VALIDATE-COMMAND-LINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; COMMAND-LINE-SPEC
(s/def ::prefix (s/cat :identifier #{"-p" "--prefix"}
                       :prefix     string?))
(s/def ::vendor (s/cat :identifier #{"-v" "--vendor"}
                       :vendor     string?))
(s/def ::flag (s/alt :prefix ::prefix
                     :vendor ::vendor))
(s/def ::flags (s/* ::flag))
;;; this has to be done with an s/and instead of just using ::api/command directly
;;; inside s/cat since ::api/command also uses a s/and which leads to spec not recognising
;;; that its actually also a s/cat thing.
(s/def ::command-line (s/and (s/cat :flags   ::flags
                                    :command (s/+ string?))
                             (s/keys :req-un [::api/command])))

(defmulti conformed->flag first)
(defmethod conformed->flag :prefix [[_ v]]
  (:prefix v))
(defmethod conformed->flag :vendor [[_ v]]
  (:vendor v))
(defn conformed->flags [conformed]
  (reduce (fn [acc v] (assoc acc (first v) (conformed->flag v))) {} conformed))

(defn validate-command-line-job
  "Returns the flags and command (in that order), so transform needs to take 3 arguments!"
  [command-line transform]
  (jobs/new-job ::validate-command-line {:command-line command-line,
                                         :transform    transform}))

(defmethod jobs/do-job! ::validate-command-line [job acc]
  (let [command-line (-> job jobs/aux :command-line)
        transform    (-> job jobs/aux :transform)
        conformed    (s/conform ::command-line command-line)]
    (if (= conformed ::s/invalid)
      (jobs/return acc [(io/conform-error-job "Something went wrong while reading the command line arguments:"
                                              (s/explain-data ::command-line command-line))])
      (jobs/return (transform acc
                              (conformed->flags (:flags conformed))
                              (api/conformed->command (:command conformed)))
                   []))))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MAIN
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn starting-jobs [args]
  [print-banner-job
   (validate-command-line-job args #(-> {} (assoc :flags %2) (assoc :cmd %3)))
   (jobs/instance-job api/command-job :flags :cmd)])

(defn -main
  "I don't do a whole lot ... yet, or do I ?."
  [& args]
  (let [starting-jobs (starting-jobs args)]
    (jobs/job-loop starting-jobs)))
