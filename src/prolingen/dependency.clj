(ns prolingen.dependency
  (:require [prolingen.jobs :as jobs]
            [prolingen.io  :as io]
            
            [clojure.string :as str]
            [clojure.java.io :as jo]
            [clojure.spec.alpha :as s]
            [clojure.pprint :as print]))

(def ^:dynamic *default-site* "repo1.maven.org/maven2")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; DEPENDENCY-SPEC
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(s/def ::dep              (s/tuple   symbol? string?))
(s/def ::dependencies     (s/coll-of ::dep))
(s/def ::source           string?)
(s/def ::license          string?)
(s/def ::url              string?)
(s/def ::vendor           symbol?)
(s/def ::vendors          (s/coll-of ::vendor))
(s/def ::module           symbol?)
(s/def ::modules          (s/coll-of ::module))
(s/def ::file             string?)
(s/def ::files            (s/coll-of ::file))
(s/def ::dependency-entries (s/keys* :req-un [::vendors ::files]
                                     :opt-un [::source ::license ::url ::dependencies ::modules]))
(s/def ::dependency (s/cat :dep? #{'defdependency}
                        :name     symbol?
                        :version  string?
                        :entries  ::dependency-entries))

(defmulti conformed->entry (fn [k v] k))
(defmethod conformed->entry :default [k v]
  v)
(defmethod conformed->entry :vendors [k v]
  (mapv keyword v))
(defmethod conformed->entry :name    [k v]
  (name v))
(defmethod conformed->entry :modules [k v]
  (mapv name v))
(defmethod conformed->entry :dependencies [k deps]
  (mapv (fn [[nam  version]] {:name   (name nam), :version version,
                             :origin (namespace nam)}) deps))
#_(defmethod conformed->entry :files [k v]
  v)

(defn conformed->dependency [conformed]
  (as-> conformed %
    (dissoc % :dep?)
    (merge (dissoc % :entries) (:entries %))
    (reduce-kv (fn [acc k v] (assoc acc k (conformed->entry k v))) {} %)))

(defmulti entry->conformed (fn [k v] k))
(defmethod entry->conformed :default [k v]
  v)
(defmethod entry->conformed :vendors [k v]
  (mapv symbol v))
(defmethod entry->conformed :name    [k v]
  (symbol v))
(defmethod entry->conformed :modules [k v]
  (mapv symbol v))
(defmethod entry->conformed :dependencies [k deps]
  (mapv (fn [dep] [(symbol (:origin dep) (:name dep)) (:version dep)]) deps))
#_(defmethod entry->conformed :files    [k v]
  (mapv (fn [file] (if (sequential? file) [:with-alias file] [:no-alias file])) v))

(defn dependency->conformed [dependency]
  (as-> dependency %
    (reduce-kv (fn [acc k v] (assoc acc k (entry->conformed k v))) {} %)
    (assoc % :entries (-> % (dissoc :name) (dissoc :version)))
    (select-keys % [:name :version :entries])
    (assoc % :dep? 'defdependency)))

(defn conformed->unformed [conformed]
  (->> conformed (s/unform ::dependency)))

(defn unformed->conformed [unformed]
  (s/conform ::dependency unformed))

(defn unformed->dependency-entries [unf]
  (->> unf (s/conform ::dependencies) (conformed->entry :dependencies)))

(defn dependency-entries->unformed [entries]
  (->> entries (entry->conformed :dependencies) (s/unform ::dependencies)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; DEPENDENCY->STRING
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn- header-str [name version]
  (str "(defdependency " name " \"" version "\""))

(defmulti entry-str (fn [k v] k))
(defmethod entry-str :default [k v]
  (let [string (with-out-str
                 (print/pprint {k v}))
        len    (.length string)]
    ;; We remove the first character and the last two, since they are guaranteed
    ;; to be '{' and '}\n' respectively (which we dont want here!)
    (subs string 1 (- len 2))))
(defmethod entry-str :run [k run-entries]
  (let [unformed (map #(s/unform ::run-entry %) run-entries)
        pretty   (mapv vec unformed)
        string (with-out-str
                 (print/pprint {k pretty}))
        len    (.length string)]
    ;; We remove the first character and the last two, since they are guaranteed
    ;; to be '{' and '}\n' respectively (which we dont want here!)
    (subs string 1 (- len 2))))

(defn- entries-str [entries]
  (let [strings (for [[k v] entries] (entry-str k v))]
    (as-> strings %
      (clojure.string/join "\n" %)
          ;;give them a small indent
      (clojure.string/replace % #"\n" "\n  "))))

(defn dependency->string [dependency]
  (let [conformed (dependency->conformed dependency)]
    (str (header-str (:name conformed) (:version conformed))
         "\n  "
         (entries-str (:entries conformed))
         ")")))

(defn dependency->string-job [dependency acc-transform]
  (jobs/new-job ::dependency->string {:dependency dependency,
                                   :transform acc-transform}))

(defmethod jobs/do-job! ::dependency->string [job acc]
  (let [dependency (-> job jobs/aux :dependency)
        transform (-> job jobs/aux :transform)]
    (jobs/return (transform acc (dependency->string dependency)) [])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; DEPENDENCY->PATH
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn dependency->path
  ([dep] (dependency->path io/repository-path dep))
  ([rep-path dep] (io/add-to-path rep-path [(:name dep) (:version dep)] :dir)))
