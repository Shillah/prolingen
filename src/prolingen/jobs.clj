(ns prolingen.jobs
  (:require [clojure.string :as str]
            [clojure.set    :as set]))

;;; a job is a map with the following structure
;;; { ::type the job type,
;;;   ::aux      auxilarry information,
;;; }

(defn new-job [type aux]
  {::type type,
   ::aux aux})

(defn jtype [job]
  (get job ::type))

(defn aux [job]
  (get job ::aux))

(defn- desc [job]
  (cond
    (nil? job)     "No Job",
    (= ::error job) "Error",
    :else          (-> job jtype name)))

(defn print-job
  "`string` is a string that is supposed to be printed."
  [string]
  (new-job  ::print {:string string}))

(defn ask-question-job
  "`acc-transform` is a function that takes the current accumulator and the response, and returns
  the new accumulator.
  [CAUTION] This job changes `acc`."
  [string transform-acc]
  (new-job ::question {:string string,
                       :transform transform-acc}))

(defn input-job
  "`read-line` but in job form.
  [CAUTION] This job changes `acc`."
  [acc-transform]
  (new-job ::input {:transform acc-transform}))

(defn error-job
  "`string` is a string describing the error. `values` is data that can help the programmer
  find out whats wrong (its not used logic wise)."
  ([string] (error-job string nil))
  ([string values] (new-job ::error-msg {:string string,
                                         :values values,
                                         :throw  true})))

(defn terminate-job []
  (new-job ::terminate {:throw true}))

(defn warning-job
  "`string` is a string that describes the warning. `level` a number corresponding to the warning level.
  `values` is data thats relevant."
  ([string values] (warning-job 1 string values))
  ([level string values]  (new-job  ::warning {:string string,
                                               :values values,
                                               :level  level})))

(defn ignore-job
  "do nothing."
  ([string values] (new-job ::ignore nil)))

(defn insert-acc-job
  "Replaces the current accumulator with `new-acc`. Same as `restore-acc-job`.
  Use depends on intent."
  [new-acc]
  (new-job ::insert new-acc))

(defn restore-acc-job [old-acc]
  "Replaces the current accumulator with `new-acc`. Same as `insert-acc-job`.
  Use depends on intent."
  (new-job ::restore old-acc))

(defn update-acc-job
  "Update the current accumulator by applying `update` on it and replacing
  it with the result."
  [update]
  (new-job ::update update))

(defn transform-aux
  "Changes the auxillary information of `job` by replacing it with the result of `transform` applied on the
  old auxillary information of `job`."
  [job transform]
  (new-job (jtype job)
           (transform (aux job))))

(defn if-job
  ([pred if]      (if-job pred if []))
  ([pred if else] (new-job ::if {:pred pred, :if if, :else else})))

(defn cond-job [& args]
  (let [pred-pairs (partition 2 args)]
    (if (every? #(= (count %) 2) pred-pairs)
      (new-job ::cond pred-pairs)
      (error-job "Tried to create cond-job with uneven number of arguments."))))

(defn instance-job
  "Instances a `new-job` by calling it with '(f acc)' for every 'f' in `args`, where acc is the accumulator."
  [new & args]
  (new-job ::instance {:new new,
                       :args args}))

(defn already-instanced
  "Looks weird but its useful. To be used with `instance-job`. This makes it easy to have arguments
  that dont depend on 'acc'."
  [val] (fn [_] val))

(def +ERROR-JOBS+ #{::error-msg,
                    ::terminate})

(defn error-job? [job]
  (+ERROR-JOBS+ (jtype job)))

(defn error? [job]
  (= job ::error))

(defmulti do-job! (fn [job input] (get job ::type)))

(defn return
  "Return from job."
  ([acc] (return acc []))
  ([acc job-list]
   {:acc acc,
    :jobs (vec job-list)}))

(defn return-with
  "When you just want to use the result of another job. Doesnt do anything, but looks good."
  [res]
  res)

(defmethod do-job! :default [job acc]
  (let [type (desc job)]
    (return acc [(error-job (str "Job type '" type "' is not registered.") {:type type})])))

(defmethod do-job! ::print [job acc]
  (let [string (-> job aux :string)]
    (println string)
    (return acc [])))

(defmethod do-job! ::input [job acc]
  (let [transform (-> job aux :transform)]
    (return (transform acc (read-line)) [])))

(defmethod do-job! ::question [job acc]
  (let [string (-> job aux :string)
        transform (-> job aux :transform)]
    (return acc [(print-job string)
                 (input-job transform)])))

(defmethod do-job! ::error-msg [job acc]
  (let [string (-> job aux :string)
        throw  (-> job aux :throw)
        pjob   (print-job (str "[ERROR] " string))]
    (if throw
      (return acc [pjob ::error])
      (return acc [pjob]))))

(defmethod do-job! ::terminate [job acc]
  (let [throw (-> job aux :throw)]
    (if throw
      (return acc [::error])
      (return acc []))))

(defmethod do-job! ::warning [job acc]
  (let [string (-> job aux :string)
        level  (-> job aux :level)
        pjob   (print-job (into (str "[WARNING (" level ")] " string)))]
    (return acc [pjob])))

(defmethod do-job! ::ignore [job acc]
  (return acc []))

(defmethod do-job! ::insert [job acc]
  (let [new-acc (aux job)]
    (return new-acc [])))

(defmethod do-job! ::restore [job acc]
  (let [old-acc (aux job)]
    (return old-acc [])))

(defmethod do-job! ::update [job acc]
  (let [update (aux job)]
    (return (update acc) [])))

(defmethod do-job! ::if [job acc]
  (let [pred (-> job aux :pred)
        if   (-> job aux :if)
        else (-> job aux :else)]
    (if (pred acc)
      (return acc if)
      (return acc else))))

(defmethod do-job! ::cond [job acc]
  (let [pred-pairs (-> job aux)
        pair       (first (filter (fn [[pred todo]] (pred acc)) pred-pairs))
        todo       (if (nil? pair) [] (second pair))]
    (return acc todo)))

(defmethod do-job! ::instance [job acc]
  (let [new  (-> job aux :new)
        args (-> job aux :args)
        ;; get the real args by applying each arg to acc
        instanced-args (map #(%1 acc) args)]
    (return acc [(apply new instanced-args)])))

(defn process-next-job [data]
  (let [acc   (:acc data)
        jobs  (:jobs data)
        job   (first jobs)
        to-do (vec (rest jobs))]
    (if (or (nil? job)
            (error? job))
      (assoc data :jobs nil)
      (let [{new-acc :acc,
             new-jobs :jobs} (do-job! job acc)]
        ;; We deliberately "overwrite" the keys in data here instead of creating
        ;; a new map here, since we want to support nonstandard "data"
        ;; that has additional keys needed for other purposes.
        (-> data
            (assoc :acc   new-acc)
            (assoc :jobs (into new-jobs to-do)))))))

(defn no-jobs?
  "Checks wether data still contains jobs that need to be handled."
  [data]
  (or (nil? (:jobs data))
      (= [] (:jobs data))))

(defn insert-marker
  ([pos data] (insert-marker pos ::halt data))
  ([pos marker data]
   (assert (<= pos (count (:jobs data))))
   (let [jobs (:jobs data)
         first-half (take pos jobs)
         second-half  (drop pos jobs)
         new-jobs (vec (concat first-half [marker] second-half))]
     (assoc data :jobs new-jobs))))

(defn process-until-marker
  ([data] (process-until-marker ::halt data))
  ([marker data]
   (loop [data data]
     (if (no-jobs? data)
       (:acc data)
       (if (= (first (:jobs data))
              marker)
         (update data :jobs #(vec (rest %)))
         (let [new-data (process-next-job data)]
           (recur new-data)))))))

(defn initial-data
  "Bundles the job queue and the acc into one data structure for easier handling."
  ([jobs] (initial-data jobs nil))
  ([jobs acc] {:acc acc, :jobs jobs}))

(defn job-loop [starting-jobs]
  (loop [data (initial-data starting-jobs)]
    (if (no-jobs? data)
      (:acc data)
      (let [new-data (process-next-job data)]
        (recur new-data)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;; JOB-DEBUG-LOOP
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; stack is just a way to make the repl less cluttered.
;;; it basically hides a vector by wrapping it into a function.
(defn- make-stack []
  (fn [] nil))

(defn- pop-stack [stack]
  (let [list (stack)]
    (fn [] (rest list))))

(defn- peek-stack [stack]
  (let [list (stack)]
    (first list)))

(defn- push-stack [stack var]
  (let [list (stack)]
    (fn [] (conj list var))))

;;;------------------------------------------------------------------------------
(defn should-stop? [stops data]
  (contains? stops (first (:jobs data))))

(defn initial-step-data [jobs]
  (assoc (initial-data jobs) :past (make-stack)))

(defn step-forwards [data]
  (let [new-data (process-next-job data)]
    (update new-data :past push-stack (dissoc data :past))))

(defn step-backwards [data]
  (if-let [old-data (peek-stack (:past data))]
    (assoc old-data :past (pop-stack (:past data)))
    data))

(defn job-debug-continue
  ([continue-data] (job-debug-continue continue-data #{::error}))
  ([continue-data stops]
   (loop [data continue-data]
     (cond
       (no-jobs? data) data
       (should-stop? stops data) (update data :jobs #(-> % rest vec))
       ;; This has to be done like this since
       ;; you cannot recur inside a try
       :else (let [new-data (try
                              (step-forwards data)
                              (catch Exception e
                                (do
                                  (println "[ERROR] CAUGHT EXCEPTION")
                                  (assoc data :exception e))))]
               (if (:exception new-data)
                 new-data
                 (recur new-data)))))))

(defn step-marker
  ([data] (step-marker data ::halt))
  ([data marker] (job-debug-continue data #{::error marker})))

(defn job-debug-loop [starting-jobs]
  (job-debug-continue (initial-step-data starting-jobs)))


