(ns prolingen.prolog
  "A collection of helper functions for prolog."
  (:require [prolingen.io :as io]
            [prolingen.jobs :as jobs]
            [clojure.string :as str]))

;;; This basically matches ':- module(name)' and ':- module(name, [export_list])'
;;; with some optional whitespace. IMPORTANT: the first group is always the name!
;;; Maybe this should also require the declaration to be on a new line.
;;; A module can be any atom.
;;; Since writing a regex for exactly an atom is hard i match the following superset instead:
;;; a nonempty sequence on nonwhitespace characters
;;; OR
;;; any sequence bounded by two "'"
;;; this will for example match a-b even though thats not an atom for many prolog systems.
;;; IMPORTANT NOTE: '' is a legal module name. thats why I used '.*' and not '.+'.
;;;                 Dont change it!
(def +MODULE-REGEX+ #"\s*:-\s?module\(\s?(\S+|'.*')\s?(,.+)?\s?\)")
(defn module-name
  "Scans `str` for the first occurence of a module definition. Returns the name of that module.
  If no module is found, `nil` is returned instead."
  [str]
  (let [matcher (re-matcher +MODULE-REGEX+ str)]
    ;; since matcher contains exactly one group,
    ;; every hit will be a pair [match group].
    ;; we are only interested in the groups.
    (second (re-find matcher))))

(defn is-prolog-file?
  "Returns `true` is path is the path to a prolog file."
  [path]
  (when (= :file (io/path-type path))
    (str/ends-with? (last (io/path-data path)) ".pl")))

(defn make-module-set-job
  "`module-map` is a map of filename->module-name?. This returns a set of all module-names.
  Throws an error if there are two modules with the same name."
  [module-map transform]
  (jobs/new-job ::make-set {:map module-map, :transform transform}))

(defn- clash-string [clash]
  (let [module (first clash)
        files  (second clash)
        files-str (str/join ", " files)]
    (str "The module '" module "' is defined by " files-str)))

(defn- error-string [clashes]
  (let [clash-strings (map clash-string clashes)
        clashes-string (str/join "\n" clash-strings)]
    (str "There are modules in your project that are defined in multiple files:\n" clashes-string)))

(defmethod jobs/do-job! ::make-set [job acc]
  (let [map (-> job jobs/aux :map)
        transform (-> job jobs/aux :transform)
        module-set (disj  (set (vals map)) nil) ; since we use 'nil' to say "no module", we need to remove it here
        module->filenames (reduce-kv (fn [m k v] (if v
                                                  (update m v conj k)
                                                  m)) {} map)
        module-clashes (filter (fn [[k v]] (> (count v) 1)) module->filenames)]
    (if (empty? module-clashes)
      (jobs/return (transform acc module-set) [])
      (jobs/return acc [(jobs/error-job (error-string module-clashes)
                                        {:job job,
                                         :set module-set,
                                         :clashes module-clashes,
                                         :module->filenames module->filenames})]))))

(defn module-declaration-from-paths-job
  "Returns the set of all modules declared in the prolog files in `paths`."
  [paths transform]
  (jobs/new-job ::get-modules {:paths paths, :transform transform}))

(defmethod jobs/do-job! ::get-modules [job acc]
  (let [paths (-> job jobs/aux :paths)
        transform (-> job jobs/aux :transform)
        prolog-paths (filter is-prolog-file? paths)
        load-jobs  (mapv #(io/load-file-job % (fn [acc content] (assoc acc (io/path->name %) content))) prolog-paths)]
    ;; load-jobs is a vector so we can just use 'into' to append the other jobs
    (jobs/return {} (into load-jobs [(jobs/update-acc-job #(reduce-kv (fn [m k v] (assoc m k (module-name v))) {}  %))
                                     (jobs/instance-job make-module-set-job identity (jobs/already-instanced #(identity %2)))
                                     (jobs/update-acc-job #(transform acc %))]))))
