(ns prolingen.download
  (:require [prolingen.io   :as io]
            [prolingen.jobs :as jobs]
            [prolingen.dependency :as dep]
            [prolingen.create-dependency :as create]

            [clj-jgit.porcelain :as git]
            
            [clojure.java.io :as jo]
            [clojure.string :as str]
            [clojure.spec.alpha :as s]))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; DEFINITIONS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def +DEFAULT-LOOKUP+ "maven")

(s/def ::name     symbol?)
(s/def ::version  string?)
(s/def ::dep-name string?)
(s/def ::site     keyword?)
(s/def ::arg      string?)
(s/def ::args     (s/coll-of ::arg))

(s/def ::input-entry (s/keys :req-un [::name ::version]))
(s/def ::entry       (s/keys :req-un [::dep-name ::site ::args ::version ::input-entry]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; GIT-DOWNLOAD
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; GIT-REPO
(defn- git-repo-job [name url dir transform & {:keys [exception]
                                          :or   {exception jobs/error-job}}]
  (jobs/new-job ::git-repo {:url url, :dir dir, :transform transform,
                            :exception exception, :name name}))

(defmethod jobs/do-job! ::git-repo [job acc]
  (let [url (-> job jobs/aux :url)
        dir (-> job jobs/aux :dir)
        nme (-> job jobs/aux :name)
        ex  (-> job jobs/aux :exception)
        tr  (-> job jobs/aux :transform)]
    (try
      (let [repo (git/git-clone url :dir dir)]
        (jobs/return (tr acc repo) []))
      (catch Exception e
        (jobs/return acc [(ex (str "We encountered an unexpected exception while downloading the git dependency '"
                                   nme "' (" (.getMessage e) ").") {:exception e, :url url, :dir dir})])))))

;;; GIT-CHECKOUT
(defn- git-checkout-job [name repo version & {:keys [exception]
                                              :or   {exception jobs/error-job}}]
  (jobs/new-job ::git-checkout {:repo repo, :version version, :exception exception,
                                :name name}))

(defmethod jobs/do-job! ::git-checkout [job acc]
  (let [repo    (-> job jobs/aux :repo)
        name    (-> job jobs/aux :name)
        version (-> job jobs/aux :version)
        ex      (-> job jobs/aux :exception)]
    (try
      (git/git-checkout repo :name version)
      (jobs/return acc [])
      (catch Exception e
        (jobs/return acc [(ex (str "We encountered an unexpected exception while downloading the git dependency '"
                                   name "' (" (.getMessage e) ").") {:exception e, :repo repo, :version version})])))))



;;; GIT-DOWNLOAD
(defn- git-download-job [rep url name full-name version transform]
  (jobs/new-job ::git-download {:rep rep, :url url, :name name, :version version,
                                :transform transform, :full-name full-name}))

(defmethod jobs/do-job! ::git-download [job acc]
  (let [rep       (-> job jobs/aux :rep)
        url       (-> job jobs/aux :url)
        name      (-> job jobs/aux :name)
        full      (-> job jobs/aux :full-name)
        version   (-> job jobs/aux :version)
        transform (-> job jobs/aux :transform)
        dep-path  (io/add-to-path rep [name version])
        git-path  (io/add-to-path dep-path ["git"])
        git-dir   (io/path->name git-path)]
    (jobs/return acc [(git-repo-job full url git-dir #(identity %2))
                      (jobs/instance-job git-checkout-job (jobs/already-instanced full)
                                         identity (jobs/already-instanced version))
                      (create/project->dependency-job dep-path git-path version)
                      (jobs/update-acc-job #(transform % git-path))])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MAVEN-DOWNLOAD
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn maven-download-job [rep site entry transform])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; DOWNLOAD
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; DOWNLOAD*
(defmulti download-job* (fn [rep entry transform] (:site entry)))
(defmethod download-job* :default [rep entry transform]
  (if (:site entry)
    (jobs/error-job (str "'" (:site entry) "' is not yet supported.") {:entry entry, :rep rep, :site (:site entry)})
    (jobs/error-job "An unexpected error occured while trying to download a dependency." {:entry entry, :rep rep, :site (:site entry)})))

(defmethod download-job* :github [rep entry transform]
  ;; git jobs expect the dep to be formated like "site.user/dep-name"
  ;; as such args should contain exactly 1 string
  (let [args (:args entry)]
    (if (= (count args) 1)
      (let [user     (first args)
            dep-name (:dep-name entry)
            full-name (str (:name (:input-entry entry)))
            url      (str "https://github.com/" user "/" dep-name ".git")
            version  (:version entry)]
        (git-download-job rep url dep-name full-name version transform))
      (jobs/error-job (str "'" (:name (:input-entry entry)) "' is a github dependency, "
                           "so it should be formated like 'github.user/dep-name'.")))))

(defmethod download-job* :gitlab [rep entry transform]
  (let [args (:args entry)]
    (if (= (count args) 1)
      (let [user     (first args)
            dep-name (:dep-name entry)
            full-name (str (:name (:input-entry entry)))
            url      (str "https://gitlab.com/" user "/" dep-name ".git")
            version  (:version entry)]
        (git-download-job rep url dep-name full-name version transform))
      (jobs/error-job (str "'" (:name (:input-entry entry)) "' is a github dependency, "
                           "so it should be formated like 'github.user/dep-name'.")))))

(defmethod download-job* :maven [rep entry transform]
  (maven-download-job rep entry))

;;; DOWNLOAD
(defn download-job [rep dep]
  (let [version  (:version dep)
        dep-name (:name    dep) 
        space (or (:origin dep) +DEFAULT-LOOKUP+) ; space looks like "gitlab.user"
        parts (clojure.string/split space #"\.") ; parts looks like ["gitlab" "user"]
        site  (keyword (first parts))
        args  (rest parts)
        new-entry {:dep-name dep-name
                   :site     site,
                   :args     args,
                   :version  version
                   :input-entry dep}]
    (download-job* rep new-entry #(identity %2))))
