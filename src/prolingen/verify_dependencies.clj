(ns prolingen.verify-dependencies
  (:require [prolingen.jobs :as jobs]
            [prolingen.io :as io]
            [prolingen.dependency :as dep]
            [prolingen.download :as download]
            
            [clojure.string :as str]
            [clojure.java.io :as jo]
            [clojure.spec.alpha :as s]
            [clojure.set        :as set]))

(defn dep-path [rep dep]
   (dep/dependency->path rep dep))
(defn load-path [rep dep]
  (let [path (dep-path rep dep)]
    (io/add-to-path path ["src"] :dir)))
(defn config-path [rep dep]
  (let [path (dep-path rep dep)]
    (io/add-to-path path ["dependency.clj"] :file)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; CYCLE-CHECK
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn edge-set [edges]
  (reduce (fn [acc [start ends]]
            (set/union acc (set (map #(vector start %) ends)))) #{} edges))

(defn remove-edges-ending-in [edges vert]
  (reduce-kv (fn [acc start ends] (assoc acc start (disj ends vert))) {} edges))

(defn remove-edge [edge-set start end]
  (update edge-set start disj end))

(defn is-black? [vert edge-map black-set]
  (let [neighbors (get edge-map vert #{})]
    (set/subset? neighbors black-set)))

(defn cycle-error-job [start walked ]
  (let [cycle-str (loop [cycle (list start)
                         current start]
                    (let [n (get walked current)]
                      (if (= n start)
                        (map str (conj cycle n))
                        (recur (conj cycle n)
                               n))))]
    (jobs/error-job (str "prolingen has detected a dependency cycle:\n"
                         (str/join " ->\n" cycle-str)) {:start start,
                                                        :walked walked})))

(defn cycle-check*-job [white grey black walked to-walk edges]
  (jobs/new-job ::cycle-check {:white white, :grey grey, :black black, :walked walked, :to-walk to-walk,
                               :edges edges}))



(defmethod jobs/do-job! ::cycle-check [job acc]
  (let [white   (-> job jobs/aux :white)
        grey    (-> job jobs/aux :grey)
        black   (-> job jobs/aux :black)
        walked  (-> job jobs/aux :walked)
        to-walk (-> job jobs/aux :to-walk)
        edges   (-> job jobs/aux :edges)]
    (cond
      (not (empty? grey))  (let [vert (first grey)]
                             (if (is-black? vert edges black)
                               ;; add vert to black
                               (let [white   white
                                     grey    (rest grey)
                                     black   (conj black vert)
                                     walked  walked
                                     to-walk (remove-edges-ending-in to-walk vert)]
                                 (jobs/return acc [(cycle-check*-job white grey black walked to-walk edges)]))
                               ;; get the next vert to visit and add him to grey
                               (let [next-vert (first (to-walk vert))] ; next-vert cannot be black, since we always remove black
                                                                       ; vertices from to-walk!
                                 (if (contains? (set grey) next-vert)
                                   ;; OHO! We found a cycle. Not Good!
                                   (jobs/return acc [(cycle-error-job next-vert (assoc walked next-vert vert))])
                                   ;; if next-vert is not grey, it has to be white!
                                   (let [white   (disj white next-vert)
                                         grey    (conj grey next-vert)
                                         black   black
                                         walked  (assoc walked next-vert vert)
                                         to-walk (remove-edge to-walk vert next-vert)]
                                       (jobs/return acc [(cycle-check*-job white grey black walked to-walk edges)]))))))
      (not (empty? white)) (let [vert (first white)]
                             (if (is-black? vert edges black)
                               ;; add vert to black
                               (let [white   (disj white vert)
                                     grey    grey
                                     black   (conj black vert)
                                     walked  walked
                                     to-walk (remove-edges-ending-in to-walk vert)]
                                 (jobs/return acc [(cycle-check*-job white grey black walked to-walk edges)]))
                               ;; add vert to grey
                               (let [white   (disj white vert)
                                     grey    (conj grey vert)
                                     black   black
                                     walked  (assoc walked vert :start) ; this is optional. mostly for debug reasons
                                     to-walk to-walk]
                                   (jobs/return acc [(cycle-check*-job white grey black walked to-walk edges)]))))
      :else                    (jobs/return acc []))))

(defn cycle-check-job
  "`edge-map` is a map representing edges of a directed graph.
  An edge A->B is represented by the key A being mapped to a set containing B.
  Throws an error if a cylce is detected. This is basically a DFS algorithm."
  ;; the white/grey/black set idea is taken from https://www.youtube.com/watch?v=rKQaZuoUR4M
  [edge-map]
  (jobs/new-job ::start-cycle-check {:edges edge-map}))

(defmethod jobs/do-job! ::start-cycle-check [job acc]
  (let [edges (-> job jobs/aux :edges)
        verts  (set/union (set (keys edges)) (reduce set/union #{} (vals edges)))]
    (jobs/return acc [(cycle-check*-job verts '() #{} {} edges edges)])))

;;; same stuff but in non job form
#_(defmethod jobs/do-job! ::cycle-check [job acc]
  (let [edges (-> job jobs/aux :edges)
        vert  (set/union (set (keys edges)) (reduce set/union #{} (vals edges)))]
    (loop [white-set vert
           grey-set '() ; the order matters here, we want to conj to the front
           black-set #{}
           walked-edges {}
           to-walk edges]
      (cond
        (not (empty? grey-set))  (let [vert (first grey-set)]
                                   (if (is-black? vert edges black-set)
                                     (recur white-set (rest grey-set) (conj black-set vert)
                                            walked-edges (remove-edges-ending-in to-walk vert))
                                     (let [next-vert (first (to-walk vert))]
                                       (if (contains? (set grey-set) next-vert)
                                         ;; OHO! We found a cycle. Not Good!
                                         (jobs/return acc [(cycle-error-job)])
                                         (recur (disj white-set next-vert) (conj grey-set next-vert) black-set
                                                (assoc walked-edges next-vert vert) (remove-edge to-walk vert next-vert))))))
        (not (empty? white-set)) (let [vert (first white-set)]
                                   (let [white-set (disj white-set vert)]
                                     (if (is-black? vert edges black-set)
                                       (recur white-set grey-set (conj black-set vert)
                                              walked-edges (remove-edges-ending-in to-walk vert))
                                       (recur white-set (conj grey-set vert) black-set
                                              (assoc walked-edges vert :start) to-walk))))
        :else                    (jobs/return acc []))
      )))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; VERIFY-DEPENDENCIES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; READ-DEPENDENCY
(defn- read-dependency-job
  [dependency-string transform]
  (jobs/new-job ::read {:string dependency-string,
                        :transform transform}))

(defmethod jobs/do-job! ::read [job acc]
  (let [string (-> job jobs/aux :string)
        transform (-> job jobs/aux :transform)]
    (try
      (let [data  (read-string string)
            error (s/explain-data ::dep/dependency data)]
        ;; s/explain-data returns nil if there is no error (otherwise it returns a map!)
        (if error
          (jobs/return acc [(io/conform-error-job (str "An error occured while reading the dependency data:") error)])
          (let [conformed  (s/conform ::dep/dependency data)
                dependency (dep/conformed->dependency conformed)]
            (jobs/return (transform acc dependency) []))))
      (catch Exception e
        (jobs/return acc [(jobs/error-job (str "An exception occured while reading the dependency data: " (.getMessage e))
                                          {:exception e, :job job, :acc acc, :string string})])))))

;;; LOAD-DEPENDENCY
(defn- download-and-load-job
  "Downloads the dependency and loads it into memory."
  [rep dep transform]
  (jobs/new-job ::download+load {:rep rep, :dep dep, :transform transform}))

(defmethod jobs/do-job! ::download+load [job acc]
  (let [dep (-> job jobs/aux :dep)
        rep (-> job jobs/aux :rep)
        transform (-> job jobs/aux :transform)
        dep-path (config-path rep dep)]
    (jobs/return acc [(jobs/print-job (str "Downloading " (:name dep) " " (:version dep) "..."))
                      (download/download-job rep dep)
                      (jobs/print-job (str "... Finished!"))
                      (io/load-file-job dep-path transform)])))

(defn- load-dependency-job
  "Returns both the updated dependency-version map and the updated module-set.
  If the dependency doesnt exist, it tries to download it."
  [rep dep transform]
  (jobs/new-job ::load {:rep rep, :dep dep, :transform transform}))

(defmethod jobs/do-job! ::load [job acc]
  (let [rep       (-> job jobs/aux :rep) 
        dep       (-> job jobs/aux :dep)
        transform (-> job jobs/aux :transform)
        download-job (download-and-load-job rep dep #(identity %2))
        dep-path  (config-path rep dep)]
    ;; try to load the dependency. if it doesnt work, try downloading it instead
    (jobs/return acc [(io/load-file-job dep-path #(identity %2) :file-not-found (constantly download-job))
                      (jobs/instance-job read-dependency-job identity (jobs/already-instanced #(transform acc %2)))])))

;;; CHECK-FILES
(defn- check-files-job
  "Checks wether all files were downloaded."
  [rep dependency]
  (jobs/new-job ::check-files {:rep rep, :dep dependency}))
(defmethod jobs/do-job! ::check-files [job acc]
  ;; TODO: check files!
  (let [dep              (-> job jobs/aux :dep)
        rep              (-> job jobs/aux :rep)
        files            (:files dep)
        file->path       (fn [fname] (io/add-to-path (load-path rep dep) [fname] :file))
        jobs             (map #(io/path-exists?-job (file->path %) :wrong-type jobs/error-job) files)]
    (jobs/return acc jobs)))

;;; CHECK-VIABLE-VENDORS
(defn- check-viable-vendors-job
  "Returns an updates set of vendors consisting of all those who are supported by dependency.
  If the set would be empty, it throws an error instead."
 [dependency vendors transform]
  (jobs/new-job ::check-vendors {:dep dependency,
                                 :vendors vendors
                                 :transform transform}))
(defmethod jobs/do-job! ::check-vendors [job acc]
  (let [dep (-> job jobs/aux :dep)
        viable-vendors (-> job jobs/aux :vendors)
        transform    (-> job jobs/aux :transform)
        rest-vendors (reduce (fn [acc vendor-of-dep] (if (contains? viable-vendors vendor-of-dep)
                                                      (conj acc vendor-of-dep)
                                                      acc))
                             #{} (:vendors dep))]
    (if (empty? rest-vendors)
      (jobs/return acc [(jobs/error-job (str "There are currently no prolog vendors that can run with the given dependencies.")
                                        {:dep dep
                                         :dep-vendors (:vendors dep),
                                         :viable-vendors viable-vendors})])
      (jobs/return (transform acc rest-vendors) []))))

;;; CHECK-MODULES
(defn- check-module-job
  "Checks wether `module` is already in use. If it is, an error is thrown. Otherwise it returns
  an updated `module-set`."
  [dependency module module-set transform]
  (jobs/new-job ::check-module  {:dep dependency, :mod module, :mods module-set, :transform transform}))
(defmethod jobs/do-job! ::check-module [job acc]
  (let [dep       (-> job jobs/aux :dep)
        mod       (-> job jobs/aux :mod)
        mods      (-> job jobs/aux :mods)
        transform (-> job jobs/aux :transform)
        name      (:name dep)
        ;; mods is a  map module-name -> dependency-name that uses it.
        used-by   (mods mod)]
    ;; Since we first check wether the dependency was already loaded before we check wether all modules are 'fresh', we dont
    ;; need to consider the case, where a module name is already in use, but by the same dependency. That means we can assume,
    ;; that we have a module name clash whenever we try to reserve a module name that is already used.
    (if used-by
      ;; we have a module name clash
      (jobs/return acc [(jobs/error-job (str "Both dependencies '" name "' and '" used-by "' try to use the module name '"
                                             mod "'.")
                                        {:mod mod, :mods mods, :dep-name name,
                                         :dep dep})])
      ;; we dont have a module name clash
      (jobs/return (transform acc (assoc mods mod name)) []))))

(defn- check-modules-job
  "Checks wether there are any module clashes with `dependency` given that all module names in `module-set` are
  already in use by other modules."
  [dependency module-set transform]
  (jobs/new-job ::check-modules {:dep dependency, :mods module-set, :transform transform}))
(defmethod jobs/do-job! ::check-modules [job acc]
  (let [dep       (-> job jobs/aux :dep)
        mods      (-> job jobs/aux :mods)
        transform (-> job jobs/aux :transform)
        name      (:name dep)
        check-jobs (mapv #(jobs/instance-job check-module-job
                                             (jobs/already-instanced dep)
                                             (jobs/already-instanced %)
                                             identity
                                             (jobs/already-instanced (fn [acc new-mods] new-mods)))
                         (:modules dep))]
    ;; We return with 'mods' as acc, since we need to guarantee, that the acc is actually
    ;; an (updated) 'mods' before we return it. This only comes up if '(:modules dep)'
    ;; is empty.
    (jobs/return mods (into check-jobs
                            [(jobs/update-acc-job #(transform acc %))]))))

;;; CHECK-DEPENDENCIES
(defn- check-dependency-job
  "Check wether everything is alright with `dependency`.  If everything went well, this returns
  an updates `dependency->version`, `module-set` and `vendors`."
  [repository-path dependency dependency->version module-set vendors transform]
  (jobs/new-job ::check-dep {:rep repository-path, :dep dependency, :dep->ver dependency->version,
                             :mods module-set, :transform transform, :vendors vendors}))

(defmethod jobs/do-job! ::check-dep [job acc]
  (let [dep       (-> job jobs/aux :dep)
        rep       (-> job jobs/aux :rep)
        dep->ver  (-> job jobs/aux :dep->ver)
        mods      (-> job jobs/aux :mods)
        vendors   (-> job jobs/aux :vendors)
        transform (-> job jobs/aux :transform)
        name      (:name dep)
        version   (:version dep)
        checked-version (dep->ver name)]
    (cond
      ;; if the dependency is already checked we dont need to do anything
      (and checked-version
           (= version checked-version)) (jobs/return (transform acc dep->ver mods vendors) [])
      ;; if the dependency is already checked, but with a different version, we have an error
      (and checked-version
           (not= version checked-version)) (jobs/return acc [(jobs/error-job
                                                              (str "Tried to load both version '" version "' and version '"
                                                                   checked-version "' of '" name "'.")
                                                              {:loaded-version checked-version,
                                                               :version        version,
                                                               :dep            dep})])
      ;; if the dependency is not checked yet, we check it out!
      :else (jobs/return {} [(check-files-job rep dep)
                             (check-viable-vendors-job dep vendors #(assoc %1 :vendors %2))
                             (check-modules-job dep mods #(assoc %1 :mods %2))  ; return the updated module-set
                             (jobs/update-acc-job #(transform acc (assoc dep->ver name version) (:mods %) (:vendors %)))]))))

(defn- check-dependencies-job [repository-path dependencies dependency->version module-set vendors graph transform]
  (jobs/new-job ::check-deps {:rep repository-path, :deps dependencies, :dep->ver dependency->version, :vendors vendors
                              :mods module-set, :graph graph, :transform transform}))
(defmethod jobs/do-job! ::check-deps [job acc]
  (let [deps      (-> job jobs/aux :deps)
        rep       (-> job jobs/aux :rep)
        dep->ver  (-> job jobs/aux :dep->ver)
        mods      (-> job jobs/aux :mods)
        vendors   (-> job jobs/aux :vendors)
        graph     (-> job jobs/aux :graph)
        transform (-> job jobs/aux :transform)

        check-deps  (mapv #(jobs/instance-job check-dependency-job
                                              (jobs/already-instanced rep)
                                              (jobs/already-instanced %)
                                              :dep->ver
                                              :mods
                                              :vendors
                                              (jobs/already-instanced (fn [acc new-dep->ver new-mod vendors]
                                                                        {:dep->ver new-dep->ver,
                                                                         :mods new-mod,
                                                                         :vendors vendors})))
                          deps)
        return-job  (jobs/update-acc-job (fn [acc]
                                           (let [dep->ver (:dep->ver acc)
                                                 mods     (:mods     acc)
                                                 vendors  (:vendors  acc)
                                                 ;; version and (dep->ver name) differ only in 2 situations:
                                                 ;; 1) dep was not checked yet
                                                 ;; 2) a conflicting (i.e. different) version of dep was already loaded
                                                 ;; We dont want to remove dep in both cases, since in 2) we have to throw
                                                 ;; an error anyways
                                                 ;; but we dont need to do that here. We can wait until the next round.
                                                 ;; it makes the code much cleaner!
                                                 to-load      (remove (fn [dep] (let [name    (:name dep)
                                                                                     version (:version dep)]
                                                                                 (= version (dep->ver name))))
                                                                      (mapcat :dependencies deps))
                                                 new-graph (reduce (fn [acc dep]
                                                                     (assoc acc (:name dep)
                                                                            (->> dep :dependencies (map :name) set)))
                                                                   graph deps)]
                                             ;; "return"
                                             (transform acc to-load dep->ver mods vendors new-graph))))
        jobs        (into check-deps [return-job])]
    (jobs/return {:dep->ver dep->ver, :mods mods, :vendors vendors} (into [] jobs))))

;;; VERIFY-DEPENENCIES
(defn- verify-deps-job [repository-path dependencies dependency->version module-set vendors
                        dependency-graph transform]
  (jobs/new-job ::verify-deps* {:rep repository-path, :deps dependencies,
                                :dep->ver dependency->version, :mods module-set,
                                :graph dependency-graph,
                                :transform transform, :vendors vendors}))

(defmethod jobs/do-job! ::verify-deps* [job acc]
  (let [rep       (-> job jobs/aux :rep)
        deps      (-> job jobs/aux :deps)
        mods      (-> job jobs/aux :mods)
        dep->ver  (-> job jobs/aux :dep->ver)
        vendors   (-> job jobs/aux :vendors)
        graph     (-> job jobs/aux :graph)
        transform (-> job jobs/aux :transform)
        load-jobs (mapv #(load-dependency-job rep % conj) deps)
        check-job #(check-dependencies-job rep % dep->ver mods vendors graph (fn [acc to-load dep->ver mods vendors graph]
                                                                               {:to-load to-load, :dep->ver dep->ver,
                                                                                :mods mods, :vendors vendors,
                                                                                :graph graph}))]
    ;; The idea is that to check wether a dependency is alright, we first check wether its ok interally, i.e. only the
    ;; dependency on its own, and then externally, i.e. wether there is no conflict with the other dependencies.
    (jobs/return [] (vec (concat load-jobs
                                 ;; acc is now a vector of all loaded dependencies
                                 [(jobs/instance-job check-job identity)
                                  (jobs/if-job #(not-empty (:to-load %))
                                               [(jobs/instance-job verify-deps-job
                                                                   (jobs/already-instanced rep)
                                                                   :to-load
                                                                   :dep->ver
                                                                   :mods
                                                                   :vendors
                                                                   :graph
                                                                   (jobs/already-instanced (fn [_ dep->ver mods vendors graph]
                                                                                             {:dep->ver dep->ver,
                                                                                              :mods     mods,
                                                                                              :vendors  vendors,
                                                                                              :graph    graph})))])
                                  (jobs/update-acc-job #(transform acc (:dep->ver %) (:mods %) (:vendors %) (:graph %)))])))))


(defn verify-dependencies-job
  "Searches repository-path for the dependencies, verifying that all their files exist,
  that the dependency.clj matches the requested dependency and that they dont have conflicting
  defined modules & versions. This also checks all dependencies of the dependencies. Returns
  the list of all dependencies."
  [repository-path dependencies reserved-modules viable-vendors transform]
  (jobs/new-job ::verify-deps {:rep repository-path,
                               :deps dependencies,
                               :mods reserved-modules,
                               :vendors viable-vendors,
                               :transform transform}))


(defmethod jobs/do-job! ::verify-deps [job acc]
  (let [rep (-> job jobs/aux :rep)
        deps (-> job jobs/aux :deps)
        reserved-mods (-> job jobs/aux :mods)
        transform     (-> job jobs/aux :transform)
        vendors       (-> job jobs/aux :vendors)
        mods          (reduce (fn [acc v] (assoc acc v (symbol "your project"))) {} reserved-mods)]
    (jobs/return acc [(verify-deps-job rep deps {} mods vendors {} (fn [acc dep->ver mods vendors graph]
                                                                     {:dep->ver dep->ver, :mods mods, :vendors vendors,
                                                                      :graph graph}))
                      (jobs/instance-job cycle-check-job :graph)
                      (jobs/update-acc-job (fn [map]
                                             (transform acc (mapv (fn [[k v]] {:name k, :version v})
                                                                  (:dep->ver map)))))])))
