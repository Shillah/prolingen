(ns prolingen.interface.menu
  (:require [prolingen.jobs :as jobs]))

(defn simple-menu
  "A simple menu is a function that just executes the action corresponding to
  the first string in the argument list its given.

  actions - a map of strings to actions"
  ([actions] (fn [args] (let [menu-name (first args)
                                   action (get actions menu-name)]
                         (action (rest args)))))
  ([actions default] (fn [args] (let [menu-name (first args)
                                     action    (get actions menu-name)]
                                 (action (rest args))
                                 #_(default (rest args))))))

(defn simple-action [action-name]
  (fn [args]
    {:command   (keyword action-name)
     :arguments args}))

(defn simple-menus-from-tree
  "tree is a map of strings to other 'trees', or function descriptors.
  trees may optionally contain a :default key, which maps to a name of another key,
  which will get used, when no matches can be found.
  A function descriptor can either be :function, in which case a default function is created with
  `simple-action` or a function (which cannot be a map!)."
  [tree]
  (let [recurse (fn recurse [action-name tree]
                  (cond
                    (map? tree) (let [default (:default tree)
                                           no-def  (dissoc tree :default)
                                           actions (reduce-kv #(assoc % %2 (recurse %2 %3)) {} no-def)]
                                       (if (and default
                                                (actions default))
                                         (simple-menu actions (actions default))
                                         (simple-menu actions)))
                    (= tree :function) (simple-action action-name)
                    :else                  tree))]
    (recurse "" tree)))

;; the menu tree
;; - new
;; -- library
;; -- program
;; - info
;; - install
;; - upload
;; - run-tests
;; - hyperpl
(defn split-by
  "Splits the sequence whenever the predicate <f> is true."
  [f coll]
  (when-let [s (seq coll)]
    (let [v (first s)
          run (cons v (take-while (complement f) (next s)))]
      (cons run (split-by f (lazy-seq (drop (count run) s)))))))

(defn assoc-strings [hmap strings tags]
  (let [zipped (map vector tags strings)
        new-map (into hmap zipped)
        number  (count zipped)]
    (if (= number (count strings))
      ;; if we had enough tags, we return the map and the rest of the tags with nothing leftover
      [new-map (drop number tags) nil]
      ;; if we didnt have enough tags we return no tags, but some leftover pieces
      [new-map nil (drop number strings)])))

(defn load-flags
  "Returns a map build the following way:

  1) Replace all strings in <args> that begin '-' with a keyword of the same name, but without '-'.
  2) Group the strings by keywords, i.e. [\"A\", :B, \"C\", \"D\", :E] will become [(\"A\"), (:B, \"C\"), (\"D\"), (:E)]
     This will produce 3 types of groups: groups of only strings (1),
                                          groups with one keyword and some number of strings (2),
                                          groups of exactly one keyword(3).
  3) Create a map the following way: For each string a group of type (1), grab the next key from <tags> if possible,
                                     and associate it with that key, otherwise put it in the list associated to <leftover>.
                                     For each group of type (2), associate that keyword with the remaining strings.
                                     For each keyword of group (3), put it in the lis associated to <leftover>.
  
  Example: (load-flags [\"A\" \"A2\" \"-B\" \"C\" \"D\" \"-E\" \"F\", \"-G\"] [:name] :leftover)
  will return
  {:name \"A\", :leftover [\"A2\", \"D\", :G], :B \"C\", :E \"F\"}

  args - a list of strings,
  tags - a list of keys,
  leftover - optional, if not given ':leftover' is used.
  "
  ([args tags] (load-flags args tags :leftover))
  ([args tags leftover]
   (let [fixed-args (map (fn [s] (if (= (first s) \-) (keyword (subs s 1)) s)) args)
         grouped    (split-by keyword? fixed-args)]
     (loop [map {leftover nil}
            list grouped
            rest-tags tags]
       (if (seq list)
         (let [e (first list)
               r (rest list)]
           (cond
             (string? (first e)) (let [[new-map new-rest-tags lefts] (assoc-strings map e rest-tags)]
                                   (recur (update new-map leftover #(into % %2) lefts) r new-rest-tags))
             (every? keyword? e) (recur (update map leftover #(into % %2) e) r rest-tags)
             :else (recur (assoc map (first e) (next e)) r rest-tags)))
         map)))))

(defn new-program-action
  [args]
  (merge {:vendor "swipl"}
         (load-flags args [:name :vendor] :flags)
         {:command :new, :type :program}))

(defn new-library-action
  [args]
  (merge {:vendor "swipl"}
         (load-flags args [:name :vendor] :flags)
         {:command :new, :type :library}))

(defn- deep-stringify [seq]
  (if (sequential? seq)
    (mapv deep-stringify seq)
    (str seq)))

(defn- sequentialize
  "`seq` is a sequence of strings, some of which are \"[\" or \"]\"
  or at least start with those.
  This function returns basically `seq`, except strings, that when put together
  have the form \"[a b c ... ]\" are instead a subsequence [\"a\" \"b\" \"c\" ...].

  EXAMPLE: [\"[main\" \"a\" \"b\" \"]\" \"test\"] ~> [[\"main\" \"a\" \"b\"] \"test\"]"
  [seq]
  (let [as-symbols (read-string (clojure.string/join " " (concat ["["] seq ["]"])))]
    (deep-stringify as-symbols)))

(defn new-run-action
  [args]
  {:command   :run,
   :to-run    (first args)
   :arguments (rest args)})

(defn build-action [args]
  {:command :build,
   :system  (keyword (first args))
   :arguments (rest args)})

(def main-menu (simple-menus-from-tree {"new" {"library" new-library-action
                                               "program" new-program-action
                                               :default  "program"}
                                        "install"   :function
                                        "info"      :function
                                        "repl"      :function
                                        "run"       new-run-action
                                        "upload"    :function
                                        "run-tests" :function
                                        "hyperpl"   :function
                                        "test"      :function
                                        "build"     build-action
                                        :default    "info"}))


(defn chosen-command-job [command-line transform-acc]
  (jobs/new-job ::chosen-command {:cmd command-line,
                                  :transform transform-acc}))

(defmethod jobs/do-job! ::chosen-command [job acc]
  (let [cmd (-> job jobs/aux :cmd)
        transform (-> job jobs/aux :transform)]
    (jobs/return (transform acc (main-menu cmd)) [])))
