(ns prolingen.settings
  (:require [prolingen.jobs :as jobs]
            [prolingen.io :as io]
            [prolingen.vendor-api :as api]
            [prolingen.helper :as h]

            [clojure.spec.alpha :as s]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SETTINGS-SPEC
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(s/def ::vendors        (s/map-of ::h/alpha-symbol ::api/vendor))
(s/def ::default-vendor ::h/alpha-symbol)
(s/def ::locations      (s/map-of ::h/alpha-symbol ::h/alpha-string))
;;; The settings that make sense locally (so they can be overwritten)
(s/def ::base-settings   (s/keys :req-un [::vendors
                                          ::locations]))
;;; The settings that only make sense globally (so they cant be overwritten)
(s/def ::additional-global-settings (s/keys :opt-un [::default-vendor]))
(s/def ::global-settings (s/and (s/merge ::base-settings
                                         ::additional-global-settings)
                                #(if (:default-vendor %)
                                   (contains? (:vendors %) (:default-vendor %))
                                   true)))
(s/def ::additional-local-settings  (s/keys))
(s/def ::local-settings  (s/and (s/merge ::base-settings
                                         ::additional-local-settings)))

(defmulti conformed->entry (fn [k v] k))
(defmethod conformed->entry :default        [k v]
  v)
(defmethod conformed->entry :vendors        [k v]
  (reduce-kv (fn [acc vendor-name vendor-info] (assoc acc
                                                     (keyword vendor-name)
                                                     (api/conformed->vendor-info vendor-info))) {} v))
(defmethod conformed->entry :locations      [k v]
  (reduce-kv (fn [acc k v] (assoc acc (keyword k) v)) {} v))
(defmethod conformed->entry :default-vendor [k v]
  (keyword v))

(defn conformed->settings [conformed]
  (reduce-kv (fn [acc k v] (assoc acc k (conformed->entry k v))) {} conformed))

(defmulti entry->conformed (fn [k v] k))
(defmethod entry->conformed :default        [k v]
  v)
(defmethod entry->conformed :vendors        [k v]
  (reduce-kv (fn [acc vendor-name vendor-info] (assoc acc
                                                     (symbol vendor-name)
                                                     (api/vendor-info->conformed vendor-info))) {} v))
(defmethod entry->conformed :default-vendor [k v]
  (symbol v))
(defmethod entry->conformed :locations      [k v]
  (reduce-kv (fn [acc k v] (assoc acc (symbol k) v)) {} v))

(defn settings->conformed [settings]
  (reduce-kv (fn [acc k v] (assoc acc k (entry->conformed k v))) {} settings))

;;; the next function doesnt do anything actually. (even though it looks like it does!)
;;; It is just there for symmetry purposes
;;; and in case the format of the settings files change
(defn conformed->unformed
  ([conformed] (conformed->unformed false conformed))
  ([local? conformed] (if local?
                        (s/unform ::local-settings conformed)
                        (s/unform ::global-settings conformed))))

(defn- unformed->conformed
  ([unformed] (unformed->conformed false unformed))
  ([local? unformed] (if local?
                        (s/unform ::local-settings unformed)
                        (s/unform ::global-settings unformed))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LOAD-SETTINGS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn read-settings-from-string-job [string local? transform]
  (jobs/new-job ::read-settings {:string string, :local? local?, :transform transform}))

(defmethod jobs/do-job! ::read-settings [job acc]
  (let [string       (-> job jobs/aux :string)
        local?       (-> job jobs/aux :local?)
        transform    (-> job jobs/aux :transform)
        type-keyword (if local? ::local-settings ::global-settings)
        type-string  (if local? "local" "global")]
    (try
      (let [settings-data (read-string string)
            error         (s/explain-data type-keyword settings-data)]
        ;; s/explain-data returns nil if there is no error (otherwise it returns a map!)
        (if error
          (jobs/return acc [(io/conform-error-job (str "An error occured while reading the " type-string
                                                       " settings data:") error)])
          (let [conformed (unformed->conformed type-keyword settings-data)
                settings  (conformed->settings conformed)]
            (jobs/return (transform acc settings) []))))
      (catch Exception e
        (jobs/return acc [(jobs/error-job (str "An exception occured while reading the " type-string
                                               " settings data: " (.getMessage e)) {:exception e,
                                                                                    :job job,
                                                                                    :string type-string})])))))

(defn load-settings-file-job
  "[CAUTION] `{}` is returned if `file` does not exist."
  [file local? transform-acc]
  (jobs/new-job ::load-settings-file {:file file,
                                      :local? local?
                                      :transform transform-acc}))

(defmethod jobs/do-job! ::load-settings-file [job acc]
  (let [path      (-> job jobs/aux :file)
        local?    (-> job jobs/aux :local?)
        transform (-> job jobs/aux :transform)]
    (jobs/return acc [(jobs/if-job (fn [_] (.exists (io/path->file path)))
                              [(io/load-file-job    path #(identity %2))
                               (jobs/instance-job read-settings-from-string-job identity
                                                  (jobs/already-instanced local?)
                                                  (jobs/already-instanced #(transform acc %2)))]
                              [(jobs/update-acc-job #(transform %1 {}))])])))

(defn load-settings-job
  ([transform-acc]       (load-settings-job nil transform-acc))
  ([flags transform-acc] (jobs/new-job ::load-settings {:prefix    (:prefix flags),
                                                        :transform transform-acc})))

(defn with-maybe-prefix [prefix path]
  (if (nil? prefix)
    path
    (io/add-prefix path prefix)))

(defmethod jobs/do-job! ::load-settings [job acc]
  (let [transform (-> job jobs/aux :transform)
        prefix    (-> job jobs/aux :prefix)
        global-settings io/settings-path
        local-settings  (with-maybe-prefix prefix (io/as-path :file "local.clj"))]
    (jobs/return acc [(jobs/insert-acc-job {})
                      (load-settings-file-job global-settings false #(assoc %1 :gbl %2))
                      (load-settings-file-job local-settings  true  #(assoc %1 :lcl %2))
                      (jobs/update-acc-job #(io/mega-merge (:gbl %) (:lcl %)))
                      (jobs/if-job #(empty? %)
                                   [(jobs/error-job (str "No settings were found. Use 'prolingen install' to install prolingen."))
                                    (jobs/insert-acc-job acc)]  ; <- this will never get executed but its still good form to
                                                                ; restore the acc anyways.
                                   [(jobs/update-acc-job #(transform acc %))])])))
