(ns prolingen.command-api
  (:require [prolingen.commands.new     :as new]
            [prolingen.commands.info    :as info]
            [prolingen.commands.install :as install]
            [prolingen.commands.repl    :as repl]
            [prolingen.commands.run     :as run]
            [prolingen.commands.test    :as test]
            [prolingen.commands.hyperpl :as hyperpl]
            [prolingen.commands.build   :as build]
            
            [prolingen.project          :as proj]
            [prolingen.settings         :as sett]
            [prolingen.jobs             :as jobs]
            [prolingen.io               :as io]
            [prolingen.verify-dependencies :as deps]
            [prolingen.dependency       :as dep]

            [clojure.spec.alpha :as s]))

;;; apparently s/cat will not include empty stuff:
;;; (s/cat :A string? :B (s/* string?))
;;; will conform ["A"] to {:A "A"}
;;; AND NOT TO {:A "A" :B []}.
;;; this is why we use :opt-un for commands that
;;; dont necessarily expect input
(defmulti command-args :command)
(defmethod command-args "install" [_]
  (s/keys :opt-un [::install/arguments]))
(defmethod command-args "new" [_]
  (s/keys :req-un [::new/arguments]))
(defmethod command-args "info" [_]
  (s/keys :opt-un [::info/arguments]))
(defmethod command-args "repl" [_]
  (s/keys :opt-un [::repl/arguments]))
(defmethod command-args "run" [_]
  (s/keys :req-un [::run/arguments]))
(defmethod command-args "test" [_]
  (s/keys :req-un [::test/arguments]))
(defmethod command-args "hyperpl" [_]
  (s/keys :opt-un [::hyperpl/arguments]))
(defmethod command-args "build" [_]
  (s/keys :req-un [::build/arguments]))
(s/def ::valid-command (s/multi-spec command-args :command))
(s/def ::command (s/and (s/cat :command string? :arguments (s/* string?))
                        ::valid-command))

(defmulti conformed->command :command)
(defmethod conformed->command "install" [conformed]
  (-> conformed
      ;; (namespace ::keyword) should always be
      ;; "prolingen.command-api"
      ;; but for future proofing (in case anyone renames this)
      ;; i used the (namespace ::keyword) trick
      (update :command #(keyword (namespace ::keyword) %))
      (update :arguments install/conformed->arguments)))
(defmethod conformed->command "new" [conformed]
  (-> conformed
      (update :command #(keyword (namespace ::keyword) %))
      (update :arguments new/conformed->arguments)))
(defmethod conformed->command "info" [conformed]
  (-> conformed
      (update :command #(keyword (namespace ::keyword) %))
      (update :arguments info/conformed->arguments)))
(defmethod conformed->command "repl" [conformed]
  (-> conformed
      (update :command #(keyword (namespace ::keyword) %))
      (update :arguments repl/conformed->arguments)))
(defmethod conformed->command "run" [conformed]
  (-> conformed
      (update :command #(keyword (namespace ::keyword) %))
      (update :arguments run/conformed->arguments)))
(defmethod conformed->command "test" [conformed]
  (-> conformed
      (update :command #(keyword (namespace ::keyword) %))
      (update :arguments test/conformed->arguments)))
(defmethod conformed->command "hyperpl" [conformed]
  (-> conformed
      (update :command #(keyword (namespace ::keyword) %))
      (update :arguments hyperpl/conformed->arguments)))
(defmethod conformed->command "build" [conformed]
  (-> conformed
      (update :command #(keyword (namespace ::keyword) %))
      (update :arguments build/conformed->arguments)))




(defn load-path [dep]
  (let [path (dep/dependency->path dep)]
    (io/add-to-path path ["src"] :dir)))

(defn command-job [flags command]
  (jobs/new-job (:command command) {:command command
                                    :flags flags}))

(defmethod jobs/do-job! ::no-such-command [job acc]
  (let [command (-> job jobs/aux :command)]
    (jobs/return acc [(jobs/error-job (str "'" (name (:command command)) "' is not a registered command.") {:job job,
                                                                                                            :command command})])))

(defmethod jobs/do-job! ::install [job acc]
  (let [flags   (-> job jobs/aux :flags)
        command (-> job jobs/aux :command)]
    (jobs/return {:cmd command, :flags flags} [(jobs/instance-job install/install-job identity)
                                               (jobs/insert-acc-job acc)])))

(defmethod jobs/do-job! ::new [job acc]
  (let [flags   (-> job jobs/aux :flags)
        command (-> job jobs/aux :command)]
    (jobs/return {:cmd command, :flags flags} [(sett/load-settings-job flags #(assoc %1 :gbl %2))
                                               (jobs/instance-job new/new-job identity)
                                               (jobs/insert-acc-job acc)])))

#_(defmethod execute-command :zip-it [cmd-options gbl-options project]
    (cond
    (= gbl-options :no-options) please-install-job
    (= project :no-project)     no-project-job
    :else (zip/zip-job {:cmd cmd-options, :gbl gbl-options, :prj project})))

#_(defmethod execute-command :upload [cmd-options gbl-options project]
    (cond
    (= gbl-options :no-options) please-install-job
    (= project :no-project)     no-project-job
    :else (upload/upload-job {:cmd cmd-options, :gbl gbl-options, :prj project})))

(defmethod jobs/do-job! ::info [job acc]
  (let [flags   (-> job jobs/aux :flags)
        command (-> job jobs/aux :command)]
    (jobs/return {:cmd command, :flags flags} [(sett/load-settings-job flags #(assoc %1 :gbl %2))
                                               (proj/load-project-job  flags #(assoc %1 :prj %2))
                                               (jobs/instance-job info/info-job identity)
                                               (jobs/insert-acc-job acc)])))

(defmethod jobs/do-job! ::repl [job acc]
  (let [flags   (-> job jobs/aux :flags)
        command (-> job jobs/aux :command)]
    (jobs/return {:cmd command, :flags flags} [(sett/load-settings-job flags #(assoc %1 :gbl %2))
                                               (proj/load-project-job  flags #(assoc %1 :prj %2))
                                               (jobs/instance-job deps/verify-dependencies-job
                                                                  (jobs/already-instanced io/repository-path)
                                                                  #(get-in % [:prj :dependencies])
                                                                  #(get-in % [:prj :modules])
                                                                  #(hash-set (get-in % [:prj :vendor]))
                                                                  (jobs/already-instanced #(assoc %1 :all-deps %2)))
                                               (jobs/update-acc-job #(assoc % :dependency-paths (mapv load-path (:all-deps %))))
                                               (jobs/instance-job repl/repl-job identity)
                                               (jobs/insert-acc-job acc)])))

(defmethod jobs/do-job! ::run [job acc]
  (let [flags   (-> job jobs/aux :flags)
        command (-> job jobs/aux :command)]
    (jobs/return {:cmd command, :flags flags} [(sett/load-settings-job flags #(assoc %1 :gbl %2))
                                               (proj/load-project-job  flags #(assoc %1 :prj %2))
                                               (jobs/instance-job deps/verify-dependencies-job
                                                                  (jobs/already-instanced io/repository-path)
                                                                  #(get-in % [:prj :dependencies])
                                                                  #(get-in % [:prj :modules])
                                                                  #(hash-set (get-in % [:prj :vendor]))
                                                                  (jobs/already-instanced #(assoc %1 :all-deps %2)))
                                               (jobs/update-acc-job #(assoc % :dependency-paths (mapv load-path (:all-deps %))))
                                               (jobs/instance-job run/run-job identity)
                                               (jobs/insert-acc-job acc)])))

(defmethod jobs/do-job! ::test [job acc]
  (let [flags   (-> job jobs/aux :flags)
        command (-> job jobs/aux :command)]
    (jobs/return {:cmd command, :flags flags} [(sett/load-settings-job flags #(assoc %1 :gbl %2))
                                               (proj/load-project-job  flags #(assoc %1 :prj %2))
                                               (jobs/instance-job deps/verify-dependencies-job
                                                                  (jobs/already-instanced io/repository-path)
                                                                  #(get-in % [:prj :dependencies])
                                                                  #(get-in % [:prj :modules])
                                                                  #(hash-set (get-in % [:prj :vendor]))
                                                                  (jobs/already-instanced #(assoc %1 :all-deps %2)))
                                               (jobs/update-acc-job #(assoc % :dependency-paths (mapv load-path (:all-deps %))))
                                               (jobs/instance-job test/test-job identity)
                                               (jobs/insert-acc-job acc)])))

(defmethod jobs/do-job! ::hyperpl [job acc]
  (let [flags   (-> job jobs/aux :flags)
        command (-> job jobs/aux :command)]
    (jobs/return {:cmd command, :flags flags} [(sett/load-settings-job flags #(assoc %1 :gbl %2))
                                               (proj/load-project-job  flags #(assoc %1 :prj %2))
                                               (jobs/instance-job deps/verify-dependencies-job
                                                                  (jobs/already-instanced io/repository-path)
                                                                  #(get-in % [:prj :dependencies])
                                                                  #(get-in % [:prj :modules])
                                                                  #(hash-set (get-in % [:prj :vendor]))
                                                                  (jobs/already-instanced #(assoc %1 :all-deps %2)))
                                               (jobs/update-acc-job #(assoc % :dependency-paths (mapv load-path (:all-deps %))))
                                               (jobs/instance-job hyperpl/hyperpl-job identity)
                                               (jobs/insert-acc-job acc)])))

(defmethod jobs/do-job! ::build [job acc]
  (let [flags   (-> job jobs/aux :flags)
        command (-> job jobs/aux :command)]
    (jobs/return {:cmd command, :flags flags} [(sett/load-settings-job flags #(assoc %1 :gbl %2))
                                               (proj/load-project-job  flags #(assoc %1 :prj %2))
                                               (jobs/instance-job deps/verify-dependencies-job
                                                                  (jobs/already-instanced io/repository-path)
                                                                  #(get-in % [:prj :dependencies])
                                                                  #(get-in % [:prj :modules])
                                                                  #(hash-set (get-in % [:prj :vendor]))
                                                                  (jobs/already-instanced #(assoc %1 :all-deps %2)))
                                               (jobs/update-acc-job #(assoc % :dependency-paths (mapv load-path (:all-deps %))))
                                               (jobs/instance-job build/build-job identity)
                                               (jobs/insert-acc-job acc)])))
