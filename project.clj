(defproject com.gitlab.shillah/prolingen "0.1.0-SNAPSHOT"
  :description "leiningen, but for prolog"
  :url "https://gitlab.com/Shillah/prolingen"
  :license {:name "GPLv3"
            :url "https://www.gnu.org/licenses/gpl-3.0.html"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/data.json "0.2.6"]
                 [org.clojure/data.xml "0.0.8"]
                 [org.clojure/clojure "1.10.0"]
                 [clj-jgit "1.0.0-beta3"]
                 [tube-alloys/redef-methods "1.0.0-SNAPSHOT"]
                 [org.clojure/test.check "0.9.0"]]
  :main ^:skip-aot prolingen.core
  :target-path "target/%s"

  :profiles {:uberjar {:aot :all}}
  :scm {:name "git", :url "https://gitlab.com/Shillah/prolingen"})
