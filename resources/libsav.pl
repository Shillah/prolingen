% -*- mode: prolog -*-

:- module(libsav, [register_alias/1, add_alias/2, prevent_bug/2, get_io/4, compile_all/1]).

:- dynamic user:file_search_path/2.
:- multifile user:file_search_path/2.

register_alias([(Alias, Path) | T]) :- asserta(user:file_search_path(Alias, Path)), register_alias(T).
register_alias([]).

% Because of some bug, swipl does not erase the first '--' which normally means "no more flags after this"
% as such we ignore '--' if its right at the start.
prevent_bug(['--'|R], R) :- !.
prevent_bug(A, A).

get_io(Args, I, O, R) :- get_io(inputs, Args, I, O, R).
get_io(inputs, [Pre, Input, '-'|Re], [I|IR], O, R) :- !, I =.. [Pre, Input], get_io(inputs, Re, IR, O, R).
get_io(inputs, [I, '-'|Re], [I|IR], O, R) :- !, get_io(inputs, Re, IR, O, R).
get_io(inputs, [Pre, Input, '--'|Re], [I], O, R) :- !, I =.. [Pre, Input], get_io(output, Re, I, O, R).
get_io(inputs, [I, '--'|Re], [I], O, R) :- !, get_io(output, Re, I, O, R).
get_io(output, [Pre, Output, '--'|Re], _, O, Re) :- !, O =.. [Pre, Output].
get_io(output, [O, '--'|Re], _, O, Re) :- !.

compile_all([]).
compile_all([I|R]) :- compile(I), compile_all(R).

add_alias([], []).
add_alias([Alias, Path|Rest], [(Alias, Path)|RestW]) :- add_alias(Rest, RestW).
