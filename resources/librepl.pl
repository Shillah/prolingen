% -*- mode: prolog -*-

:- module(librepl, [register_alias/1, add_alias/2, prevent_bug/2]).

:- dynamic user:file_search_path/2.
:- multifile user:file_search_path/2.

register_alias([(Alias, Path) | T]) :- asserta(user:file_search_path(Alias, Path)), register_alias(T).
register_alias([]).

%add_alias([H|T], Out) :- add_alias(H, T, Out).
%add_alias(Current, [H | T], [(Current, H) | Out]) :- H \= '--', !, add_alias(Current, T, Out).
%add_alias(_, [ -- , Next | T], Out) :- !, add_alias(Next, T, Out).
%add_alias(_, [], []).

% Because of some bug, swipl does not erase the first '--' which normally means "no more flags after this"
% as such we ignore '--' if its right at the start.
% Bug occured on version: SWI-Prolog version 8.1.29 for x86_64-linux
prevent_bug(['--'|R], R) :- !.
prevent_bug(A, A).

add_alias([], []).
add_alias([Alias, Path|Rest], [(Alias, Path)|RestW]) :- add_alias(Rest, RestW).
