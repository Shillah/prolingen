% -*- mode: prolog -*-
% This is your main file

:- module(main, [count/2]).

count([], 0).
count([_|R], X) :- count(R, Y), X is Y + 1.

main(Args) :- count(Args, X),
              write("Hello, World! #project-name is here!"), nl,
              write("Thanks for the "), write(X), write(" arguments!"), nl.
