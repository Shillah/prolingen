% -*-  mode: prolog -*-



% call like <alias1> <path1> <alias2> <path2> ...

:- use_module(librepl, []).

:- current_prolog_flag(argv, Args),
   librepl:prevent_bug(Args, NArgs),
   librepl:add_alias(NArgs, WithAlias),
   librepl:register_alias(WithAlias).
