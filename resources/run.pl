% -*- mode: prolog -*-


% Call like so: swipl --quiet -l <this file> -
% Module Goal Argnum -- Alias1 Path1 Alias2 Path2 ... -- File1 - File2 - File3 ... -- Argument1 Argument2 ...
% Module may be omitted, FileX can be AliasX FileX instead.
% An example call:
% main 1 -- library <some path> src <another path> -- src main.pl - other.pl -- 1 2 3

% be careful: Sicstus is dogshit. it changes the cwd while consulting a file,
% to the directory the file is in.  That means all filenames should be absolute.

:- use_module(librun, []).

:- current_prolog_flag(argv, Args),
   librun:setup_run(Args, Files, Goal, GoalArgs),
   librun:consult_all(Files),
   librun:try_goal(Goal, GoalArgs),
   halt.
