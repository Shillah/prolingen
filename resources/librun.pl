% -*- mode: prolog -*-

:- module(librun, [setup_run/4, consult_all/1, try_goal/2]).

:- dynamic user:file_search_path/2.
:- multifile user:file_search_path/2.

register_alias([(Alias, Path) | T]) :- asserta(user:file_search_path(Alias, Path)), register_alias(T).
register_alias([]).

% I = input, G = Goal, P = paths, F = files, A = arguments
cut(I, G, P, F, A) :- cut(bug_prevention, I, G, P, F, A).
% Because of some bug, swipl does not erase the first '--' which normally means "no more flags after this"
% as such we ignore '--' if its right at the start.
% Bug occured on version: SWI-Prolog version 8.1.29 for x86_64-linux
cut(bug_prevention, ['--'|Rest], G, P, F, A) :- !, cut(goal, Rest, G, P, F, A).
cut(bug_prevention, I, G, P, F, A) :- !, cut(goal, I, G, P, F, A).
cut(goal, [Goal, Argnum, '--'|Rest], (user, Goal, Argnum), P, F, A) :- !, cut(paths, Rest, _, P, F, A).
cut(goal, [Module, Goal, Argnum, '--'|Rest], (Module, Goal, Argnum), P, F, A) :- !, cut(paths, Rest, _, P, F, A).
cut(paths, ['--'|Rest], _, [], F, A) :- !, cut(files, Rest, _, _, F, A).
cut(paths, [Alias, Path|Rest], _, [(Alias, Path)|RestP], F, A) :- !, cut(paths, Rest, _, RestP, F, A).
cut(files, ['--'|Rest], _, _, [], A) :- !, cut(args, Rest, _, _, _, A).
cut(files, [File, '--'|Rest], _, _, [File], A) :- !, cut(args, Rest, _, _, _, A).
cut(files, [Alias, File, '--'|Rest], _, _, [(Alias,File)], A) :- !, cut(args, Rest, _, _, _, A).
cut(files, [File, '-'|Rest], _, _, [File|RestF], A) :- !, cut(files, Rest, _, _, RestF, A).
cut(files, [Alias, File, '-'|Rest], _, _, [(Alias,File)|RestF], A) :- !, cut(files, Rest, _, _, RestF, A).
cut(args, Rest, _, _, _, Rest).

setup_run(Args, Files, Goal, GoalArgs) :- cut(Args, Goal, Paths, Files, GoalArgs),
                                          %add_alias(Paths, WithAlias),
                                          register_alias(Paths).

%%% IMPORTANT: We use user:consult(..) to force all loaded files, which are _not_ module files, into the user module.
%%%            Otherwise they would be loaded into the librun module and had access to its private functions, such as
%%%            register_alias etc.
consult_all([]).
consult_all([(Alias, Name)|T]) :- !, Term =.. [Alias, Name], user:consult(Term), consult_all(T).
consult_all([H|T]) :- user:consult(H), consult_all(T).

try_goal((Module, Goal, '0'), _)    :- !, call(Module:Goal).
try_goal((Module, Goal, '1'), Argv) :- !, call(Module:Goal, Argv).
