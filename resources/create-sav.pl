% -*-  mode: prolog -*-

:- use_module(libsav, []).

:- current_prolog_flag(argv, Args),
   libsav:prevent_bug(Args, NArgs),
   libsav:get_io(NArgs, Inputs, Output, NArgs2),
   libsav:add_alias(NArgs2, WithAlias),
   libsav:register_alias(WithAlias),
   libsav:compile_all(Inputs),
   save_program(Output),
   halt.
